import numpy as np
from scipy.optimize import NonlinearConstraint
from scipy.optimize import LinearConstraint
from scipy.optimize import Bounds
from scipy.optimize import minimize
from CFR import CFR
from ExtensiveGame import ExtensiveGame
from math import exp


class SequenceQSE():
    def __init__(self, fname):
        # save game name
        self.fname = fname

        # Game variables
        self.game = ExtensiveGame()
        self.game.load(fname)

        # Variables to save
        self.is_to_node = {}
        self.action_index = {}
        self.action = 1
        self.i_set = 1
        self.i_sets = set()
        self.rationality = None

        # Constraints
        self.constraints = None
        self.nonlinear_constraints = None
        self.x0 = None
        self.bounds = None

        # Just information variable
        self.nodes = 0

        # Saved solution in object
        self.solution = None

    def solve(self, x0=None, rationality=1, method="SLSQP"):
        self.rationality = rationality
        self.nodes = 0
        self.create_variables()
        self.create_expresions()
        if x0 is not None:
            self.x0 = x0
        self.solution = minimize(self.objective, self.x0, method=method, bounds=self.bounds,
                                 constraints=[self.constraints, self.nonlinear_constraints])
        return self.solution

    def create_variables(self):
        self.create_variables_step(self.game.root, (-1, -1), 0, 1.0)

    def create_variables_step(self, node, last_node_action, last_node_strat, chance):
        self.nodes += 1
        if node.player == 3:
            # Terminal node
            if last_node_action not in self.is_to_node:
                self.is_to_node[last_node_action] = []
            self.is_to_node[last_node_action].append((node, chance, last_node_strat))
        elif node.player == 2:
            # Chance node
            for child, next_chance in zip(node.children, node.chance):
                self.create_variables_step(child, last_node_action, last_node_strat, chance * next_chance)
        elif node.player == 0:
            # Player 1
            if last_node_action not in self.is_to_node:
                self.is_to_node[last_node_action] = []
            if node.i_set not in self.is_to_node[last_node_action]:
                self.is_to_node[last_node_action].append(node.i_set)
                self.i_sets.add(node.i_set)
            for i, child in enumerate(node.children):
                self.create_variables_step(child, (node.i_set, i), last_node_strat, chance)
        else:
            # Player 2 (my agent)
            create_strats = False
            if (last_node_strat, node.i_set) not in self.action_index:
                self.action_index[(last_node_strat, node.i_set)] = []
                create_strats = True
            for i, child in enumerate(node.children):
                if create_strats:
                    self.action_index[(last_node_strat, node.i_set)].append(self.action)
                    self.action += 1
                self.create_variables_step(child, last_node_action,
                                           self.action_index[((last_node_strat, node.i_set))][i],
                                           chance)

    def objective(self, x):
        # print(x[self.action])
        return x[self.action]

    def random_strategy(self):
        constrain_matrix = self.constraints.A
        x0 = [0] * len(constrain_matrix[0])
        x0[0] = 1
        for i in range(1, len(constrain_matrix)):
            parent_val = 0
            b = sum(constrain_matrix[i])
            temp_strat = np.random.uniform(0, 1, b + 1)
            temp_strat = temp_strat / sum(temp_strat)
            index = 0
            for j in range(len(constrain_matrix[i])):
                if constrain_matrix[i][j] == -1:
                    parent_val = x0[j]
                if constrain_matrix[i][j] == 1:
                    x0[j] = temp_strat[index] * parent_val
                    index += 1
        return x0

    def create_expresions(self):
        size = self.action + len(self.i_sets) + 1
        # print("Actions:", self.action)
        # print("Information sets:", len(self.i_sets))
        # print("Nodes:", self.nodes)
        adition = 1
        const_array = []
        lesser = []
        greater = []
        app = [0] * size
        app[0] = 1
        const_array.append(app)
        lesser.append(1)
        greater.append(1)

        for key, value in self.action_index.items():
            app = [0] * size
            app[key[0]] = -1
            for val in value:
                app[val] = 1
            const_array.append(app)
            lesser.append(0)
            greater.append(0)

        self.constraints = LinearConstraint(const_array, lesser, greater)
        # print(self.constraints.A)

        # print(lesser)
        # print(const_array)
        # print(greater)
        self.x0 = [0] * size
        lb = [-np.inf] * size
        ub = [np.inf] * size
        for i in range(self.action):
            lb[i] = 0
            ub[i] = 1
        self.bounds = Bounds(lb, ub)

        recreated = {}
        for key, value in self.is_to_node.items():
            if key[0] not in recreated:
                recreated[key[0]] = []
            recreated[key[0]].append(value)

        ret = "def f(x):\n  return ["
        # print(recreated)
        for key, value in recreated.items():
            if key == -1:
                key_mod = self.action
            else:
                key_mod = key + adition + self.action
            ret += "-x[" + str(key_mod) + "] "
            top = "("
            bot = "("
            for action_values in value:
                expr = ""
                for single_value in action_values:
                    if isinstance(single_value, int):
                        expr += "+x[" + str(single_value + adition + self.action) + "] "
                    else:
                        expr += "+" + str(single_value[0].value) + "*" + str(
                            single_value[1]) + "*x[" + str(single_value[2]) + "] "
                top += "+(" + expr + ")*exp(" + str(self.rationality) + "*(" + expr + "))"
                bot += "+exp(" + str(self.rationality) + "*(" + expr + "))"
            ret += "+" + top + ")/" + bot + "),"
        ret = ret.strip(",")
        ret += "]"
        exec(ret, globals())
        # print(ret)
        self.nonlinear_constraints = NonlinearConstraint(f, 0, 0)

    def strategy_in_cfr_format(self):
        if self.solution is None:
            print("Instance not yet solved.")
        else:
            cfrqr = CFR(self.fname)
            cfrqr.solve(1)
            x = self.solution['x']
            for key, value in self.action_index.items():
                temp = [0] * len(value)
                i = 0
                if x[key[0]] < 0.000001:
                    temp = [1 / len(value)] * len(value)
                else:
                    for val in value:
                        temp[i] = x[val] / x[key[0]]
                        i += 1
                cfrqr.strategy[1][key[1]] = temp
            return cfrqr.strategy
