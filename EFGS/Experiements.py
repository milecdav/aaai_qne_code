from CFR import CFR
from CFRBR import CFRBR
from CFRQRCFV import CFRQRCFV
from CFRQRNORM import CFRQRNORM
from SequenceNash import SequenceNash
from DataManipulation import *
from Plot import *
import numpy as np
from RQR import RQR
from BestNE import BestNE
from SequenceQSE import SequenceQSE
from CFRLin import CFRLin
from ADAPTQR import ADAPTQR
import glob


# computes set convergence curves and prints horizontal lines where nash br and nash qr values are
def plot_convergence_curves_with_nash_values(fname, algorithm, iterations=1000, cfr_plus=False, cfr_player=1,
                                             rationality=1, current=True, average=True, br=True, qr=True,
                                             logscale_y=False, logscale_x=False, lines=True, skip=1, p=0.5,
                                             plot_qse=True):
    if algorithm == "cfr":
        cfr_alg = CFR(fname, cfr_plus=cfr_plus)
        qr = False
    elif algorithm == "cfrbr":
        cfr_alg = CFRBR(fname, cfr_player=cfr_player)
        qr = False
    elif algorithm == "cfrqrcfv":
        cfr_alg = CFRQRCFV(fname, cfr_player=cfr_player, rationality=rationality)
    elif algorithm == "cfrqrnorm":
        cfr_alg = CFRQRNORM(fname, cfr_player=cfr_player, rationality=rationality)
    elif algorithm == "rqr":
        cfr_alg = RQR(fname, cfr_player=cfr_player, rationality=rationality, p=p)
    elif algorithm == "rqrcfv":
        cfr_alg = RQR(fname, cfr_player=cfr_player, rationality=rationality, p=p, norm=False)
    elif algorithm == "cfrlin":
        cfr_alg = CFRLin(fname, cfr_player=cfr_player, rationality=rationality)
        qr = False
    else:
        raise ValueError("Wrong algorithm specification, use one of: cfr, cfrbr, cfrqrcfv, cfrqrnorm")
    cfr_alg.solve(iterations=iterations, verbose=3, save_progression=True, skip=skip)

    curve = None
    if current:
        if average:
            if br:
                if qr:
                    if algorithm == "cfr" or algorithm == "cfrbr":
                        raise AttributeError("cfr and cfrbr does not have qr curves")
                    curve = create_plotable_progression(cfr_alg.progression)
                else:
                    curve = create_plotable_progression_both_br(cfr_alg.progression)
            else:
                if qr:
                    if algorithm == "cfr" or algorithm == "cfrbr":
                        raise AttributeError("cfr and cfrbr does not have qr curves")
                    curve = create_plotable_progression_both_qr(cfr_alg.progression)
        else:
            if br:
                if qr:
                    if algorithm == "cfr" or algorithm == "cfrbr":
                        raise AttributeError("cfr and cfrbr does not have qr curves")
                    curve = create_plotable_progression_current_br_qr(cfr_alg.progression)
                else:
                    curve = create_plotable_progression_current_br(cfr_alg.progression)
            else:
                if qr:
                    if algorithm == "cfr" or algorithm == "cfrbr":
                        raise AttributeError("cfr and cfrbr does not have qr curves")
                    curve = create_plotable_progression_current_qr(cfr_alg.progression)
    else:
        if average:
            if br:
                if qr:
                    if algorithm == "cfr" or algorithm == "cfrbr":
                        raise AttributeError("cfr and cfrbr does not have qr curves")
                    curve = create_plotable_progression_avg_br_qr(cfr_alg.progression)
                else:
                    curve = create_plotable_progression_avg_br(cfr_alg.progression)
            else:
                if qr:
                    if algorithm == "cfr" or algorithm == "cfrbr":
                        raise AttributeError("cfr and cfrbr does not have qr curves")
                    curve = create_plotable_progression_avg_qr(cfr_alg.progression)

    if curve is None:
        raise AttributeError("Curve set is empty from your specification, set at least one "
                             "of current and average to True and at least one of br or qr to True")

    # cfr_alg.print_strategy(cfr_alg.average_strategy, 1, compact=True, decimal_points=8)

    name, axis_labels = data_for_graph_of_convergence_curve(algorithm, fname, rationality)

    seq_nash = BestNE(fname)
    seq_nash.solve(rationality=rationality)

    hlines = None
    if lines:
        hlines = [(cfr_alg.best_response(0, seq_nash.strategy_in_cfr_format())[0], "Nash equilibrium value", "b", ":")]

        if qr:
            if algorithm == "cfrqrcfv" or algorithm == "rqrcfv":
                hlines.append(
                    (cfr_alg.quantal_response(0, seq_nash.strategy_in_cfr_format(), rationality=rationality)[0],
                     "Value of QR to nash equilibrium strategy", "r", ":"))
            else:
                hlines.append((cfr_alg.normalized_quantal_response(0, seq_nash.strategy_in_cfr_format(),
                                                                   rationality=rationality)[0],
                               "Value of QR to nash equilibrium strategy", "r", ":"))

        if plot_qse:
            seq_qse = SequenceQSE(fname)
            seq_qse.solve(rationality=rationality)

            hlines.append(
                (
                    cfr_alg.best_response(0, seq_qse.strategy_in_cfr_format())[0], "Value of BR to QSE strategy", "m",
                    ":"))
            if algorithm == "cfrqrcfv" or algorithm == "rqrcfv":
                hlines.append(
                    (cfr_alg.quantal_response(0, seq_qse.strategy_in_cfr_format(), rationality=rationality)[0],
                     "Value of QSE", "y", ":"))
            else:
                hlines.append((cfr_alg.normalized_quantal_response(0, seq_qse.strategy_in_cfr_format(),
                                                                   rationality=rationality)[0],
                               "Value of QSE", "y", ":"))

    plot_convergence_curves(curve, axis_labels, name, logscale_y=logscale_y, logscale_x=logscale_x, hlines=hlines)


# computes exploitation and exploitability curves for specified algorithm
def plot_exploitation_and_exploitability(fname, algorithm, iterations=1000, cfr_player=1, rationality=1, current=True,
                                         average=True, logscale_y=False, logscale_x=False, exploitability_on_axis=False,
                                         zeroline=False):
    if algorithm == "cfrqrcfv":
        cfr_alg = CFRQRCFV(fname, cfr_player=cfr_player, rationality=rationality)
    elif algorithm == "cfrqrnorm":
        cfr_alg = CFRQRNORM(fname, cfr_player=cfr_player, rationality=rationality)
    else:
        raise ValueError("Wrong algorithm specification, use one of: cfrqrcfv, cfrqrnorm")
    cfr_alg.solve(iterations=iterations, verbose=3, save_progression=True)

    curve = None
    if current:
        if average:
            curve = create_plotable_progression(cfr_alg.progression)
        else:
            curve = create_plotable_progression_current_br_qr(cfr_alg.progression)
    else:
        if average:
            curve = create_plotable_progression_avg_br_qr(cfr_alg.progression)

    if curve is None:
        raise AttributeError("Curve set is empty from your specification, set at least one "
                             "of current and average to True and at least one of br or qr to True")

    if exploitability_on_axis:
        name, axis_labels = data_for_graph_exploitability_on_axis(algorithm, fname, rationality)
    else:
        name, axis_labels = data_for_graph_of_exploitability_curve(algorithm, fname, rationality)

    seq_nash = SequenceNash(fname)
    seq_nash.solve()

    nash_br = -seq_nash.solution

    if algorithm == "cfrqrcfv":
        nash_qr = cfr_alg.quantal_response(0, seq_nash.strategy_in_cfr_format(), rationality=rationality)[0]
    else:
        nash_qr = cfr_alg.normalized_quantal_response(0, seq_nash.strategy_in_cfr_format(), rationality=rationality)[0]

    br = True
    for line in curve:
        if br:
            line[1] = line[1].replace("Best response to", "Exploitability of")
            line[0] = -np.array(line[0]) + nash_br
        else:
            line[1] = line[1].replace("Quantal response to", "Exploitation by")
            line[0] = np.array(line[0]) - nash_qr
        br = not br
    if exploitability_on_axis:
        plot_exploitability_curves(curve, axis_labels, name, logscale_y=logscale_y, logscale_x=logscale_x)
    else:
        plot_convergence_curves(curve, axis_labels, name, logscale_y=logscale_y, logscale_x=logscale_x,
                                zeroline=zeroline)


def exploitability_and_exploitation_based_on_lambda(fname, save, algorithm, iterations=1000, cfr_player=1,
                                                    current=True, average=True, logscale_y=False, logscale_x=False,
                                                    exploitability_on_axis=False, zeroline=False, lambdas=None,
                                                    plot_everything=False, p=None, qse=True):
    if lambdas is None:
        lambdas = [0, 0.01, 0.1, 0.5, 1, 2, 5, 10, 20, 50, 100]
    data = []
    data2 = []

    seq_nash = SequenceNash(fname)
    seq_nash.solve()

    nash_br = -seq_nash.solution

    nash_qrs = []

    qse_qrs = []
    qse_brs = []

    if not (current or average):
        raise AttributeError("At least one of current or average must be true.")
    for rationality in lambdas:
        print()
        print(rationality)
        if algorithm == "cfrqrcfv":
            cfr_alg = CFRQRCFV(fname, cfr_player=cfr_player, rationality=rationality)
        elif algorithm == "cfrqrnorm":
            cfr_alg = CFRQRNORM(fname, cfr_player=cfr_player, rationality=rationality)
        elif algorithm == "rqr":
            if p is not None:
                if p < 0 or p > 1:
                    p = None
            cfr_alg = RQR(fname, cfr_player=cfr_player, rationality=rationality)
        elif algorithm == "adaptqr":
            cfr_alg = ADAPTQR(fname, cfr_player=cfr_player, rationality=rationality)
        else:
            raise ValueError("Wrong algorithm specification, use one of: cfrqrcfv, cfrqrnorm")
        if algorithm == "rqr":
            max_val = -np.inf
            if p is None:
                for ip in np.linspace(0, 1, 11):
                    print(ip)
                    rqr = RQR(fname, cfr_player=cfr_player, rationality=rationality)
                    rqr.solve(iterations=iterations, verbose=0, save_progression=False, p=ip)
                    val = rqr.quantal_response(1 - cfr_player, rqr.average_strategy, rationality)[0]
                    if val > max_val:
                        cfr_alg = rqr
                        max_val = val
            else:
                cfr_alg.solve(iterations=iterations, verbose=0, save_progression=False, p=p)
        if algorithm == "adaptqr":
            cfr_alg.solve(iterations=iterations, verbose=0, save_progression=False)
        else:
            cfr_alg.solve(iterations=iterations, verbose=0, save_progression=False)
        #
        # cfr_alg.print_strategy(cfr_alg.strategy, 1, compact=True)
        # cfr_alg.print_strategy(cfr_alg.strategy, 0, compact=True)

        cfrqr = CFRQRCFV(fname, cfr_player=cfr_player, rationality=rationality)
        cfrqr.solve(iterations=iterations, verbose=0, save_progression=False)
        data2.append(cfrqr.print_responses())

        if qse:
            seq_qse = SequenceQSE(fname)
            seq_qse.solve(rationality=rationality)
            qse_brs.append(cfr_alg.best_response(0, seq_qse.strategy_in_cfr_format())[0])
            qse_qrs.append(cfr_alg.quantal_response(0, seq_qse.strategy_in_cfr_format(), rationality=rationality)[0])

        if algorithm == "adaptqr":
            data.append(cfr_alg.return_values())
        else:
            data.append(cfr_alg.print_responses())
        if algorithm != "cfrqrnorm":
            nash_qrs.append(cfr_alg.quantal_response(0, seq_nash.strategy_in_cfr_format(), rationality=rationality)[0])
        else:
            nash_qrs.append(
                cfr_alg.normalized_quantal_response(0, seq_nash.strategy_in_cfr_format(), rationality=rationality)[0])

    save_to_file((fname, lambdas, algorithm, data, data2, nash_br, nash_qrs, qse_brs, qse_qrs),
                 "data/lambdas/" + save + ".lambdas")
    return

    curve = None
    if current:
        if average:
            curve = create_plotable_progression(data)
        else:
            curve = create_plotable_progression_current_br_qr(data)
    else:
        if average:
            curve = create_plotable_progression_avg_br_qr(data)

    if exploitability_on_axis:
        name, axis_labels = data_for_graph_exploitability_on_axis(algorithm, fname, rationality)
    else:
        name, axis_labels = data_for_graph_of_exploitability_curve_rationality(algorithm, fname, rationality)

    if not plot_everything:
        br = True
        for line in curve:
            if br:
                line[1] = line[1].replace("Best response to", "Exploitability of")
                line[0] = -np.array(line[0]) + nash_br
            else:
                line[1] = line[1].replace("Quantal response to", "Exploitation by")
                for i in range(len(line[0])):
                    line[0][i] = line[0][i] - nash_qrs[i]
            br = not br
        if exploitability_on_axis:
            plot_exploitability_curves(curve, axis_labels, name, logscale_y=logscale_y, logscale_x=logscale_x)
        else:
            plot_convergence_curves(curve, axis_labels, name, labels=[str(i) for i in lambdas], logscale_y=logscale_y,
                                    logscale_x=logscale_x, zeroline=zeroline)
    else:
        curve = curve + tuple([(nash_qrs, "Quantal response to nash strategy", "r:")])
        curve = curve + tuple([(qse_qrs, "Quantal response to qse strategy", "y-.")])
        curve = curve + tuple([([nash_br] * len(lambdas), "Best response to nash strategy", "b:")])
        curve = curve + tuple([(qse_brs, "Best response to qse strategy", "m--")])
        plot_convergence_curves(curve, axis_labels, name, labels=[str(i) for i in lambdas], logscale_y=logscale_y,
                                logscale_x=logscale_x, zeroline=zeroline)


def plot_expl_based_on_lambda_for_adapt(dir, qnefname=None, current=True, comb_val=0):
    all_data = []
    for fname in glob.glob(dir + "/*"):
        fname, lambdas, algorithm, data, data2, nash_br, nash_qrs, qse_brs, qse_qrs = load_from_file(fname)
        if qnefname is not None:
            fn, lm, alg, data2 = load_from_file(qnefname)
        if current:
            curve = [[value[2][comb_val] for value in data], "CRQR", "r--"], [
                [value[3][comb_val] for value in data], "", "b-"]
            curve2 = [[value[2] for value in data2], "CFRQR", "c--"], [
                [value[0] for value in data2], "", "y-"]
        else:
            curve = [[value[0] for value in data], "Quantal response to average strategy", "r-"], [
                [value[1] for value in data], "Best response to average strategy", "b-"]

        name, axis_labels = data_for_graph_of_exploitability_curve_rationality(algorithm, fname, 0)

        for i in range(len(curve[0][0])):
            if curve[0][0][i] > qse_qrs[i]:
                curve[0][0][i] = qse_qrs[i]
            if curve2[0][0][i] > qse_qrs[i]:
                curve2[0][0][i] = qse_qrs[i]

        curve = curve + curve2
        curve = tuple([(qse_qrs, "GA", "y--")]) + curve
        curve = tuple([(qse_brs, "", "m-")]) + curve
        curve = curve + tuple([(nash_qrs, "NASH", "r--")])
        curve = curve + tuple([([nash_br] * len(lambdas), "", "b-")])
        all_data.append(curve)

    print("Data", all_data)
    resulting_curve = all_data[0]
    print("res", resulting_curve)
    for i, rc in enumerate(resulting_curve):
        for j in range(len(rc[0])):
            print(resulting_curve[i][0][j])
            val = 0
            count = 0
            for curve in all_data:
                count += 1
                val += curve[i][0][j]
            resulting_curve[i][0][j] = val / count
    plot_convergence_curves(resulting_curve, ("Rationality", "Expected utility"), "", labels=[str(i) for i in lambdas],
                            logscale_y=False,
                            logscale_x=False, zeroline=False, font_size=13)


def plot_rqr_curves(fname, iterations, p=None, linspace_val=None, rationality=1, norm=False):
    if p is None:
        if linspace_val is None:
            linspace_val = 11
        p = np.linspace(0, 1, linspace_val)

    seq_qse = SequenceQSE(fname)
    seq_qse.solve(rationality=rationality)

    cfr = CFR(fname)
    cfr.solve(1)
    if norm:
        qseqr = cfr.normalized_quantal_response(0, seq_qse.strategy_in_cfr_format(), rationality)[0]
    else:
        qseqr = -seq_qse.solution['fun']
    qsebr = cfr.best_response(0, seq_qse.strategy_in_cfr_format())[0]

    seq_nash = BestNE(fname)
    seq_nash.solve(rationality=rationality)

    nashbr = cfr.best_response(0, seq_nash.strategy_in_cfr_format())[0]
    if norm:
        nashqr = cfr.normalized_quantal_response(0, seq_nash.strategy_in_cfr_format(), rationality)[0]
    else:
        nashqr = cfr.quantal_response(0, seq_nash.strategy_in_cfr_format(), rationality)[0]
    rqrbr = []
    rqrqr = []
    for p in p:
        print(p)
        rqr = RQR(fname, rationality=rationality)
        rqr.solve(iterations=iterations, p=p)
        rqrbr.append(rqr.best_response(0, rqr.average_strategy)[0])
        if norm:
            rqrqr.append(rqr.normalized_quantal_response(0, rqr.average_strategy, rationality)[0])
        else:
            rqrqr.append(rqr.quantal_response(0, rqr.average_strategy, rationality)[0])

    data = [
        (rqrbr, "BR to RQR alogirthm", "b-"),
        (rqrqr, "QR to RQR alogirthm", "r-"),
    ]

    name, axis_titles = data_for_graph_of_rqr("rqr", fname, rationality)

    hlines = [
        (nashqr, "QR to nash strategy", "r", "--"),
        (nashbr, "BR to nash strategy", "b", "--"),
        (qsebr, "BR to qse strategy", "m", ":"),
        (qseqr, "QR to qse strategy", "y", ":")
    ]

    plot_convergence_curves(data, axis_titles, name, hlines, vlines=True)


def save_one_alg_for_diff_lambda_values(fname, lambdas=None, cfr_player=1, iterations=1000):
    if lambdas is None:
        lambdas = [0, 0.01, 0.1, 0.5, 1, 2, 5, 10, 20, 50, 100]
    data = []
    for rationality in lambdas:
        cfrqr = CFRQRCFV(fname, cfr_player=cfr_player, rationality=rationality)
        cfrqr.solve(iterations=iterations, verbose=1, save_progression=False)
        data.append(cfrqr.print_responses())
    save_to_file((fname, lambdas, "cfrqr", data), "data/ocp_only_cfrqr.lambdas")
