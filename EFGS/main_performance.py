import numpy as np
from CFR import CFR
from CFRQRCFV import CFRQRCFV
from SequenceQSE import SequenceQSE
from SequenceNash import SequenceNash
from Combination import Combination
from ADAPTQR import ADAPTQR
import argparse
import tabulate

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='My example explanation')
    parser.add_argument(
        '-f',
        '--filename',
        default="data/small/00007.gbt",
        help='Name of game file to be used. Format efg or gbt. (default is example game located in data/small/00007.gbt)'
    )
    parser.add_argument(
        '-i',
        '--iterations',
        default=1000,
        help='Number of iterations for algorithm (default is 1000).',
        type=int
    )
    parser.add_argument(
        '-r',
        '--rationality',
        default=1,
        help='Rationality of the Quantal response opponent (default is 1).',
        type=float
    )
    parser.add_argument(
        '-g',
        '--gurobi',
        default=False,
        help='Is Gurobi available on your system (default is False)',
        type=bool
    )
    args = parser.parse_args()

    fname = args.filename
    iterations = args.iterations
    rationality = args.rationality
    gurobi = args.gurobi

    cfr = CFR(fname)
    cfr.solve(iterations)
    cfr_strat = cfr.average_strategy

    cfrqr = CFRQRCFV(fname, rationality=rationality)
    cfrqr.solve(iterations)
    cfrqr_strat = cfrqr.strategy

    qse_seq = SequenceQSE(fname)
    qse_seq.solve(rationality=rationality)
    qse_strat = qse_seq.strategy_in_cfr_format()

    combine = Combination(cfrqr_strat, cfr_strat, cfrqr)
    qr = 0
    br = 0
    max_val = -np.inf
    for s in combine.combination_space():
        qr = cfrqr.quantal_response(0, s, rationality)[0]
        if qr > max_val:
            max_val = qr
            br = cfrqr.best_response(0, s)[0]

    rqr = ADAPTQR(fname, norm=False, rationality=rationality)
    rqr.solve(iterations)
    rqr_strat = rqr.average_strategy

    if gurobi:
        seq_nash = SequenceNash(fname)
        seq_nash.solve()
        nash_strat = seq_nash.strategy_in_cfr_format()
    print("Zero-sum efg performance")
    table = [["COMB:", max_val, br],
             ["GA:", cfr.quantal_response(0, qse_strat, rationality)[0], cfr.best_response(0, qse_strat)[0]],
             ["RQR:", cfr.quantal_response(0, rqr_strat, rationality)[0], cfr.best_response(0, rqr_strat)[0]],
             ["CFR-QR:", cfr.quantal_response(0, cfrqr_strat, rationality)[0], cfr.best_response(0, cfrqr_strat)[0]],
             ["CFR:", cfr.quantal_response(0, cfr_strat, rationality)[0], cfr.best_response(0, cfr_strat)[0]]]
    if gurobi:
        table.append(["NASH:", cfr.quantal_response(0, nash_strat, rationality)[0], cfr.best_response(0, nash_strat)[0]])
    print(tabulate.tabulate(table, headers=["Algortihm", "Utility vs QR", "Utility vs BR"]))
