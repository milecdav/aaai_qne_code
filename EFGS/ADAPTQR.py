from CFR import CFR
import random
import numpy as np


class ADAPTQR(CFR):
    def __init__(self, fname, cfr_player=1, rationality=1, norm=False):
        super().__init__(fname, cfr_plus=False)
        self.cfr_player = cfr_player
        self.rationality = rationality
        self.progressive_strategy = False
        self.norm = norm
        self.p = None
        self.prev_val = 0
        self.move = 0.01
        self.decay = 1
        self.thr = 1.00001
        self.last_br = True
        self.val_avg = 0
        self.best_strategy = None
        self.best_value = -np.inf
        self.fixed = False
        self.best_combined_value = []
        self.best_combined_strategy = []
        self.best_comb_qr = []
        self.best_comb_br = []
        self.comb_numbers = 11

    def solve(self, iterations=1000, verbose=0, save_progression=False, save_strategy=False, skip=1):
        self.p = 0.5
        self.adaptqr = True
        self.decay = np.power(2, -1 / iterations)
        for i in range(self.comb_numbers):
            self.best_combined_value.append(-np.inf)
            self.best_combined_strategy.append(None)
            self.best_comb_qr.append(0)
            self.best_comb_br.append(0)
        super().solve(iterations, verbose, save_progression, save_strategy, skip)
        self.iteration = 0
        self.average_strategy = None
        self.initialize()
        self.fixed = True
        super().solve(iterations, verbose, save_progression, save_strategy, skip)

    def compute_strategy(self):
        self.compute_strategy_for_player(self.cfr_player)
        qr_val, qr = self.quantal_response(1 - self.cfr_player, self.strategy, self.rationality)
        br_val, br = self.best_response(1 - self.cfr_player, self.strategy)
        if self.best_value < qr_val:
            self.best_value = qr_val
            self.best_strategy = self.strategy
        for i, comb_number in enumerate(np.linspace(0, 1, self.comb_numbers)):
            if self.best_combined_value[i] < qr_val + comb_number * br_val:
                self.best_combined_value[i] = qr_val + comb_number * br_val
                self.best_combined_strategy[i] = self.strategy
                self.best_comb_qr[i] = qr_val
                self.best_comb_br[i] = br_val
        if not self.fixed:
            # if self.iteration > 100:
            #     move = val - self.prev_val if self.last_br else self.prev_val - val
            #     self.p -= move / self.val_avg * self.move
            # elif self.iteration < 100:
            #     self.val_avg += abs(val - self.prev_val)
            # else:
            #     self.val_avg /= 100

            up_thr_val = self.thr * self.prev_val if self.prev_val > 0 else self.prev_val / self.thr
            do_thr_val = self.prev_val / self.thr if self.prev_val > 0 else self.thr * self.prev_val
            if qr_val > up_thr_val:
                if self.last_br:
                    self.p -= self.move
                else:
                    self.p += self.move
            elif qr_val < do_thr_val:
                if self.last_br:
                    self.p += self.move
                else:
                    self.p -= self.move
            self.prev_val = qr_val
            self.move *= self.decay
        # print("p", self.p)
        r = random.random()
        if r < self.p:
            self.last_br = False
            if self.norm:
                self.strategy = self.normalized_quantal_response(1 - self.cfr_player, self.strategy, self.rationality)[
                    1]
            else:
                self.strategy = qr
        else:
            self.last_br = True
            self.strategy = br

    def compute_regret(self):
        self.compute_regret_for_player(self.cfr_player)

    def print_responses(self):
        # print()
        responses = [self.best_response(1 - self.cfr_player, self.average_strategy)[0],
                     self.best_response(1 - self.cfr_player, self.strategy)[0], [], []]
        responses[2] = self.quantal_response(1 - self.cfr_player, self.average_strategy, self.rationality)[0]
        responses[3] = self.quantal_response(1 - self.cfr_player, self.strategy, self.rationality)[0]
        # print("BR:", responses[0], end=" ")
        # print("QR:", responses[2])
        # print("BRc:", responses[1], end=" ")
        # print("QRc:", responses[3],
        #       end=" ")
        return responses

    def return_values(self):
        return self.quantal_response(1 - self.cfr_player, self.average_strategy, self.rationality)[0], \
               self.best_response(1 - self.cfr_player, self.average_strategy)[0], self.best_comb_qr, self.best_comb_br
