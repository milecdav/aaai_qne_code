This is the codebase and data sample for experiments on normal-form games.

There are four files to be run - main_speed_zero_sum.py, main_performance_zero+sum.py, main_speed_general_sum.py and main_performance_general_sum.py

main_speed_*****.py runs algorithms that we compare on one game and prints the resulting times.
main_performace_*****.py runs the same alforithms but reports expected utility against QR and BR.
All the files can be used with the same 4 arguments:
	-f, --filename 		- specifies the game file that should be loaded.
	-i, --iterations 	- specifies number of iterations for algorithms based on cfr. Default is 1000.
	-r, --rationality 	- specifies rationality of QR player. Default is 1.
	-g, --gurobi		- specifies if there is gurobi in the computer and if true, LP and MILP are run. Default is False.