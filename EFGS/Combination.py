import copy
import numpy as np


class Combination:

    def __init__(self, s1, s2, cfr):
        # print(len(s1))
        # print(len(s2))
        self.s1 = s1
        self.s2 = s2
        self.s3 = copy.deepcopy(s1)
        self.visited = []
        self.p = None
        self.cfr = cfr
        self.game = cfr.game

    def best_strategy(self, splits=11):
        space = self.combination_space(splits)

    def combination_space(self, splits=11):
        space = []
        for p in np.linspace(0, 1, splits):
            space.append(copy.deepcopy(self.combine_strategies(p)))
        return space

    def combine_strategies(self, p):
        self.p = p
        self.visited = []
        self.combine_strategy_at_node(self.game.root, [1, 1], [1, 1])
        return self.s3

    def combine_strategy_at_node(self, node, s1_reach, s2_reach):
        player = node.player
        if player == 3:
            return
        elif player == 2:
            for child in node.children:
                self.combine_strategy_at_node(child, s1_reach, s2_reach)
        else:
            for i, child in enumerate(node.children):
                new_s1_reach = [0, 0]
                new_s2_reach = [0, 0]
                new_s1_reach[1 - player] = s1_reach[1 - player]
                new_s1_reach[player] = s1_reach[player] * self.s1[player][node.i_set][i]

                new_s2_reach[1 - player] = s2_reach[1 - player]
                new_s2_reach[player] = s2_reach[player] * self.s2[player][node.i_set][i]
                self.combine_strategy_at_node(child, new_s1_reach, new_s2_reach)
            if (player, node.i_set) not in self.visited and len(node.children) > 0:
                self.visited.append((player, node.i_set))
                self.combine_in_is(player, node.i_set, s1_reach[player], s2_reach[player])

    def combine_in_is(self, player, iset, s1reach, s2reach):
        if s1reach * self.p + s2reach * (1 - self.p) == 0:
            self.set_uniform_result(player, iset)
        else:
            self.s3[player][iset] = np.divide(np.multiply(self.s1[player][iset], s1reach * self.p) + np.multiply(
                self.s2[player][iset], s2reach * (1 - self.p)), s1reach * self.p + s2reach * (1 - self.p))

    def set_uniform_result(self, player, iset):
        strategy_size = len(self.s3[player][iset])
        self.s3[player][iset] = [1 / strategy_size] * strategy_size
