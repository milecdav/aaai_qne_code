import argparse
from RQR import RQR

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='My example explanation')
    parser.add_argument(
        '-f',
        '--filename',
        default="data/small/00007.gbt",
        help='Name of game file to be used. Format efg or gbt. (default is example game located in data/small/00007.gbt)'
    )
    parser.add_argument(
        '-i',
        '--iterations',
        default=1000,
        help='Number of iterations for algorithm (default is 1000).',
        type=int
    )
    parser.add_argument(
        '-r',
        '--rationality',
        default=1,
        help='Rationality of the Quantal response opponent (default is 1).',
        type=float
    )
    parser.add_argument(
        '-p',
        '--p_value',
        default=0.5,
        help='Value of fixed p parameter in RQR (default is 0.5)',
        type=float
    )
    args = parser.parse_args()

    fname = args.filename
    iterations = args.iterations
    rationality = args.rationality
    p = args.p_value

    rqr = RQR(fname, norm=False, rationality=rationality)
    rqr.solve(iterations, p=p)
    rqr_strat = rqr.average_strategy

    print("RQR p set")
    print("Against QR:", rqr.quantal_response(0, rqr.average_strategy, rationality)[0])
    print("Against BR:", rqr.best_response(0, rqr.average_strategy)[0])
