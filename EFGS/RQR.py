from CFR import CFR
import random


class RQR(CFR):
    def __init__(self, fname, cfr_player=1, rationality=1, norm=False):
        super().__init__(fname, cfr_plus=False)
        self.cfr_player = cfr_player
        self.rationality = rationality
        self.progressive_strategy = False
        self.norm = norm
        self.p = None

    def solve(self, iterations=1000, verbose=0, save_progression=False, skip=1, p=0.5):
        self.p = p
        super().solve(iterations, verbose, save_progression, skip)

    def compute_strategy(self):
        self.compute_strategy_for_player(self.cfr_player)
        r = random.random()
        if r < self.p:
            if self.norm:
                self.strategy = self.normalized_quantal_response(1 - self.cfr_player, self.strategy, self.rationality)[
                    1]
            else:
                self.strategy = self.quantal_response(1 - self.cfr_player, self.strategy, self.rationality)[1]
        else:
            self.strategy = self.best_response(1 - self.cfr_player, self.strategy)[1]

    def compute_regret(self):
        self.compute_regret_for_player(self.cfr_player)

    def print_responses(self):
        print()
        responses = [self.best_response(0, self.average_strategy)[0], self.best_response(0, self.strategy)[0], [], []]
        responses[2] = self.quantal_response(1 - self.cfr_player, self.average_strategy, self.rationality)[0]
        responses[3] = self.quantal_response(1 - self.cfr_player, self.strategy, self.rationality)[0]
        print("BR:", self.best_response(1 - self.cfr_player, self.average_strategy)[0], end=" ")
        print("QR:", self.quantal_response(1 - self.cfr_player, self.average_strategy, self.rationality)[0])
        print("BRc:", self.best_response(1 - self.cfr_player, self.strategy)[0], end=" ")
        print("QRc:", self.quantal_response(1 - self.cfr_player, self.strategy, self.rationality)[0],
              end=" ")
        return responses
