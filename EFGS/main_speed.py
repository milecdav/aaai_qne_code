import time
from CFR import CFR
from CFRQRCFV import CFRQRCFV
from SequenceQSE import SequenceQSE
from SequenceNash import SequenceNash
from Combination import Combination
from ADAPTQR import ADAPTQR
import argparse
import tabulate

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='My example explanation')
    parser.add_argument(
        '-f',
        '--filename',
        default="data/small/00000.gbt",
        help='Name of game file to be used. Format efg or gbt. (default is example game located in data/small/00000.gbt)'
    )
    parser.add_argument(
        '-i',
        '--iterations',
        default=1000,
        help='Number of iterations for cfrlike algorithms (default is 1000).',
        type=int
    )
    parser.add_argument(
        '-r',
        '--rationality',
        default=1,
        help='Rationality of the Quantal response opponent (default is 1).',
        type=float
    )
    parser.add_argument(
        '-g',
        '--gurobi',
        default=False,
        help='Is Gurobi available on your system (default is False)',
        type=bool
    )
    args = parser.parse_args()

    fname = args.filename
    iterations = args.iterations
    rationality = args.rationality
    gurobi = args.gurobi

    cfr = CFR(fname)
    time_cfr_start = time.time()
    cfr.solve(iterations)
    time_cfr_end = time.time()
    time_cfr = time_cfr_end - time_cfr_start

    time_cfr_qr_start = time.time()
    cfrqr = CFRQRCFV(fname, rationality=rationality)
    cfrqr.solve(iterations)
    time_cfr_qr_end = time.time()
    time_cfr_qr = time_cfr_qr_end - time_cfr_qr_start

    time_qse_start = time.time()
    qse_seq = SequenceQSE(fname)
    qse_seq.solve(rationality=rationality)
    time_qse_end = time.time()
    time_qse = time_qse_end - time_qse_start

    time_comb_start = time.time()
    qne_str = cfrqr.average_strategy
    nash_str = cfr.average_strategy
    combine = Combination(qne_str, nash_str, cfrqr)
    qr = []
    br = []
    for s in combine.combination_space():
        qr.append(cfrqr.quantal_response(0, s, rationality)[0])
        br.append(cfrqr.best_response(0, s)[0])
    time_comb_end = time.time()
    time_comb = time_comb_end - time_comb_start

    time_rqr_start = time.time()
    rqr = ADAPTQR(fname, norm=False, rationality=rationality)
    rqr.solve(iterations)
    time_rqr_end = time.time()
    time_rqr = time_rqr_end - time_rqr_start

    if gurobi:
        time_nash_start = time.time()
        seq_nash = SequenceNash(fname)
        seq_nash.solve()
        time_nash_end = time.time()
        time_nash = time_nash_end - time_nash_start
    print("Zero-sum efg times")
    table = [["COMB time:", time_comb + time_cfr + time_cfr_qr],
             ["GA time:", time_qse],
             ["RQR time:", time_rqr],
             ["CFR-QR time:", time_cfr_qr],
             ["CFR time:", time_cfr]]
    if gurobi:
        table.append(["NASH time:", time_nash])
    print(tabulate.tabulate(table))
