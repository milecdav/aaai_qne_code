import math


class ExtensiveGame:

    def __init__(self):
        self.root = None
        self.chance_count = 0
        self.node_count = 0
        self.isets_p1 = {}
        self.isets_p2 = {}
        self.actual_set_p1 = 0
        self.actual_set_p2 = 0
        self.ids = 0
        self.node_names = {}

    # loads game from file that is in gambit format (expects two player zero sum game and ignores everything before
    # first tree line (first line that starts with c,t or p)
    def load(self, fname):
        with open(fname, 'r') as file:
            line = file.readline()
            while not line.startswith(("p", "c", "t")):
                line = file.readline()
            if line.startswith("c"):
                split = line.split()
                self.root = Node(2, int(split[2]), None, self.ids)
                self.ids += 1
                n_children = int((len(split) - 7) / 2)
                for i in range(n_children):
                    self.root.chance.append(float(split[6 + 2 * i]))
            else:
                split = line.split()
                if int(split[2]) == 1:
                    if split[3] in self.isets_p1:
                        next_set = self.isets_p1[split[3]]
                    else:
                        next_set = self.actual_set_p1
                        self.actual_set_p1 += 1
                        self.isets_p1[split[3]] = next_set
                else:
                    if split[3] in self.isets_p2:
                        next_set = self.isets_p2[split[3]]
                    else:
                        next_set = self.actual_set_p2
                        self.actual_set_p2 += 1
                        self.isets_p2[split[3]] = next_set
                self.root = Node(int(split[2]) - 1, next_set, None, self.ids)
                self.ids += 1
                n_children = self.get_children_line(line)
            self.load_children(self.root, n_children, file)

    def get_children_line(self, line):
        line = line[line.find("{") + 1: line.rfind("}")]
        return int(line.count("\"") / 2)

    def load_children(self, node, n_children, file):
        self.node_count += 1
        for i in range(n_children):
            line = file.readline().strip()
            if line.startswith("c"):
                split = line.split()
                next_node = Node(2, int(split[2]), node, self.ids)
                self.ids += 1
                next_children = int((len(split) - 7) / 2)
                for j in range(next_children):
                    next_node.chance.append(float(split[6 + 2 * j]))
            elif line.startswith("p"):
                split = line.split()
                if int(split[2]) == 1:
                    if split[3] in self.isets_p1:
                        next_set = self.isets_p1[split[3]]
                    else:
                        next_set = self.actual_set_p1
                        self.actual_set_p1 += 1
                        self.isets_p1[split[3]] = next_set
                else:
                    if split[3] in self.isets_p2:
                        next_set = self.isets_p2[split[3]]
                    else:
                        next_set = self.actual_set_p2
                        self.actual_set_p2 += 1
                        self.isets_p2[split[3]] = next_set
                next_node = Node(int(split[2]) - 1, next_set, node, self.ids)
                self.ids += 1
                next_children = self.get_children_line(line)
            elif line.startswith("t"):
                split = line.split()
                next_node = Node(3, None, node, self.ids, value=float(split[5].strip(",")))
                self.ids += 1
                next_children = 0
            else:
                return
            node.children.append(next_node)
            self.load_children(next_node, next_children, file)

    # prints game tree into console
    def print_tree(self):
        self.print_node(self.root, 0)

    def print_node(self, node, depth):
        for i in range(depth):
            print("-", end='')
        node.print()
        if not node.player == 3:
            for next_node in node.children:
                self.print_node(next_node, depth + 1)

    # saves game to file in gambit format
    def save_to_file(self, fname):
        self.node_count = 0
        self.chance_count = 0
        with open(fname, 'w') as file:
            file.write("EFG 2 R \"My tree\" { \"Player 1\" \"Player 2\" }\n")
            self.save_node_to_file(self.root, file, 0)

    def save_node_to_file(self, node, file, level):
        for i in range(level):
            file.write(" ")
        if node.player == 3:
            if node.value in self.node_names:
                name = self.node_names[node.value]
            else:
                name = self.node_count
                self.node_count += 1
            file.write(
                "t \"\" " + str(name - 1) + " \"\" { " + str(node.value) + " " + str(-node.value) + " }\n")
        elif node.player == 2:
            file.write("c \"\" " + str(self.chance_count) + " \"\" { ")
            self.chance_count += 1
            for i in range(len(node.children)):
                file.write("\"" + str(i) + "\" " + str(node.chance[i]) + " ")
            file.write("} 0\n")
        else:
            file.write("p \"\" " + str(node.player + 1) + " " + str(node.i_set) + " \"\" {")
            for i in range(len(node.children)):
                file.write(" \"" + str(i) + "\"")
            file.write(" } 0\n")
        if node.children is not None:
            for next_node in node.children:
                self.save_node_to_file(next_node, file, level + 1)

    # consistently gives increasing is's, for use when creating game from outside
    def getid(self):
        self.ids += 1
        return self.ids - 1

    def switch_players(self):
        self.switch_players_step(self.root)

    def switch_players_step(self, node):
        if node.player == 3:
            node.value = -node.value
        if node.player < 2:
            node.player = 1 - node.player
        for child in node.children:
            self.switch_players_step(child)


class Node:

    def __init__(self, player, i_set, parent, id_val, value=None):
        self.id_val = id_val
        self.player = player
        self.i_set = i_set
        self.parent = parent
        self.children = []
        self.chance = []
        self.value = value

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        ret = str(self.player)
        if self.player < 2:
            ret += " " + str(self.i_set)
        elif self.player == 3:
            ret += " Val:" + str(self.value)
        return "N(" + ret + " " + str(self.id_val) + ")"

    def print(self):
        print(self.player, end=" ")
        if self.player < 2:
            print(self.i_set)
        elif self.player == 3:
            print("Val:", self.value)
        else:
            print()

    def __eq__(self, other):
        return self.player == other.player and self.id_val == other.id_val and self.i_set == other.i_set

    def __hash__(self):
        return hash((self.player, self.id_val, self.i_set))
