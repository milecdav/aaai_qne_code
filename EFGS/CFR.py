from ExtensiveGame import ExtensiveGame
import numpy as np
import copy


class CFR:
    def __init__(self, fname, cfr_plus=True):
        # load the game
        self.game = ExtensiveGame()
        self.game.load(fname)

        # needed variables for algorithm
        # this will hold which game has which isets for both players
        self.i_sets = [[], []]
        # this holds which nodes belong to each iset
        self.i_sets_to_nodes = [{}, {}]
        # counterfactual values in node
        self.counterfactual_values = {}
        self.counterfactual_regret = [{}, {}]
        # strategies
        self.strategy = [{}, {}]
        self.average_strategy = None
        # actual game values
        self.game_value = 0
        self.avg_game_value = 0
        # iteration and iteration sum
        self.iteration = 0
        self.iter_count = 0
        # save of all values
        self.progression = []
        # for computing average strategies
        self.avg_visited = []
        # verbose flag
        self.verbose = 0
        # cfr plus tag
        self.cfr_plus = cfr_plus
        self.progressive_strategy = self.cfr_plus
        # response buffers for values, reaches and completed isets
        self.iset_tree = [{}, {}]
        self.response_reaches = {}
        # saving prograssion
        self.save_strategy = False
        self.save_progression = False
        self.skip = 1
        self.regret_sum_accumulator = []
        self.strategy_accumulator = {'cur': [], 'avg': []}
        self.epsilon = 0.000001
        self.adaptqr = False
        self.fixed = None

    def set_cfr_plus(self, active):
        self.cfr_plus = active

    def initialize(self):
        self.create_isets()
        self.create_iset_tree()
        self.initialize_strategy()
        self.initialize_regrets()
        self.compute_strategy()
        self.compute_game_value(self.strategy)
        self.iter_count = 0

    def solve(self, iterations=1000, verbose=0, save_progression=False, save_strategy=False, skip=1, fixed=None,
              strat=None):
        self.fixed = fixed
        self.skip = skip
        self.save_progression = save_progression
        self.save_strategy = save_strategy
        self.verbose = verbose
        self.initialize()
        if strat is not None:
            self.strategy = strat
        for self.iteration in range(iterations):
            # if self.iteration in [32, 64, 128, 256]:
            #     print(self.best_response(0, self.average_strategy)[0])
            self.iter_count += self.iteration
            if not self.perform_iteration():
                break
        return self.game_value

    def perform_iteration(self):
        self.compute_counterfactual_values()
        # print(self.strategy, "Strat")
        self.compute_regret()
        self.compute_strategy()
        self.average_strategies()
        self.print_statistics()
        # print(self.counterfactual_regret, "Regert")
        # print(self.average_strategy, "Avgstrat")
        # print(self.strategy, "Strat")
        if self.save_strategy:
            self.strategy_accumulator['cur'].append(copy.deepcopy(self.strategy))
            self.strategy_accumulator['avg'].append(copy.deepcopy(self.average_strategy))
        # if not self.adaptqr:
        # if self.iteration % 100 == 0:
        #     if self.sum_of_strategy_regret() < self.epsilon:
        #         return False
        return True

        # self.regret_sum_accumulator.append(self.sum_of_strategy_regret())

    def print_statistics(self):
        if self.verbose > 0:
            print(self.iteration, end=" ")
        if self.verbose > 1:
            self.compute_game_value(self.strategy)
            print(self.game_value, end=" ")
            self.compute_game_value(self.average_strategy)
            self.avg_game_value = self.game_value
            print(self.avg_game_value, end=" ")
        if self.verbose > 2:
            responses = self.print_responses()
            if self.save_progression:
                self.progression.append(responses)
        if self.verbose > 0:
            print()

    def print_responses(self):
        print()
        responses = [self.best_response(0, self.average_strategy)[0], self.best_response(0, self.strategy)[0]]
        print("BR:", responses[0], "BR c:", responses[1], end=" ")
        return responses

    # strategy computation
    def compute_strategy(self):
        self.compute_strategy_for_player(0)
        self.compute_strategy_for_player(1)

    def compute_strategy_for_player(self, player):
        for iset in self.i_sets[player]:
            if self.fixed is not None and iset in self.fixed[player]:
                continue
            sumed = np.sum(np.clip(self.counterfactual_regret[player][iset], 0, np.inf))
            if sumed > 0:
                for i, regret in enumerate(self.counterfactual_regret[player][iset]):
                    self.strategy[player][iset][i] = max(0, regret) / sumed
            else:
                self.set_uniform_strategy(player, iset, self.strategy)

    # regret computation
    def compute_regret(self):
        self.compute_regret_for_player(0)
        self.compute_regret_for_player(1)

    def compute_regret_for_player(self, player):
        for iset in self.i_sets[player]:
            # print("Iset", iset)
            cfv = [0] * len(self.i_sets_to_nodes[player][iset][0].children)
            for node in self.i_sets_to_nodes[player][iset]:
                for i, child in enumerate(node.children):
                    cfv[i] += self.counterfactual_values[child][player]
                    # print(self.counterfactual_values[child][player], child)
            value = 0
            for i in range(len(cfv)):
                value += cfv[i] * self.strategy[player][iset][i]
            # print(player, cfv, value)
            for i, cfv_value in enumerate(cfv):
                self.counterfactual_regret[player][iset][i] += cfv_value - value
                if self.cfr_plus:
                    self.counterfactual_regret[player][iset][i] = max(0, self.counterfactual_regret[player][iset][i])

    # computing counterfactual values
    def compute_counterfactual_values(self):
        self.counterfactual_values[self.game.root] = self.compute_cfvs(self.game.root, 1, 1)

    def compute_cfvs(self, node, reach0, reach1):
        if node.player == 3:
            self.counterfactual_values[node] = (node.value * reach0, -node.value * reach1)
            return node.value * reach0, -node.value * reach1
        elif node.player == 1:
            p0val = 0
            p1val = 0
            for i, child in enumerate(node.children):
                temp0, temp1 = self.compute_cfvs(child, reach0 * self.strategy[1][node.i_set][i], reach1)
                self.counterfactual_values[child] = (temp0, temp1)
                p0val += temp0
                p1val += temp1 * self.strategy[1][node.i_set][i]
            return p0val, p1val
        elif node.player == 0:
            p0val = 0
            p1val = 0
            for i, child in enumerate(node.children):
                temp0, temp1 = self.compute_cfvs(child, reach0, reach1 * self.strategy[0][node.i_set][i])
                self.counterfactual_values[child] = (temp0, temp1)
                p0val += temp0 * self.strategy[0][node.i_set][i]
                p1val += temp1
            return p0val, p1val
        else:
            p0val = 0
            p1val = 0
            for child, chance in zip(node.children, node.chance):
                temp0, temp1 = self.compute_cfvs(child, reach0 * chance, reach1 * chance)
                self.counterfactual_values[child] = (temp0, temp1)
                p0val += temp0
                p1val += temp1
            return p0val, p1val

    # computing game value
    def compute_game_value(self, strategy):
        backup_strategy = self.strategy
        self.strategy = strategy
        self.game_value = 0
        self.value_of_the_game(self.game.root, 1)
        self.game_value = -self.game_value
        self.strategy = backup_strategy
        return self.game_value

    def value_of_the_game(self, node, reach):
        if node.player == 3:
            self.game_value += node.value * reach
        elif node.player == 1:
            for i, child in enumerate(node.children):
                self.value_of_the_game(child, reach * self.strategy[1][node.i_set][i])
        elif node.player == 0:
            for i, child in enumerate(node.children):
                self.value_of_the_game(child, reach * self.strategy[0][node.i_set][i])
        else:
            for child, chance in zip(node.children, node.chance):
                self.value_of_the_game(child, reach * chance)

    # averaging strategies
    def average_strategies(self):
        if self.iteration > 0:
            self.avg_visited = []
            self.avg_strat_at_node(self.game.root, [1, 1], [1, 1])
        else:
            self.average_strategy = copy.deepcopy(self.strategy)

    def avg_strat_at_node(self, node, player_reach, player_average_reach):
        player = node.player
        if player == 3:
            return
        elif player == 2:
            for child in node.children:
                self.avg_strat_at_node(child, player_reach, player_average_reach)
        else:
            for i, child in enumerate(node.children):
                new_player_reach = [0, 0]
                new_player_average_reach = [0, 0]
                new_player_reach[1 - player] = player_reach[1 - player]
                new_player_reach[player] = player_reach[player] * self.strategy[player][node.i_set][i]

                new_player_average_reach[1 - player] = player_average_reach[1 - player]
                new_player_average_reach[player] = player_average_reach[player] * \
                                                   self.average_strategy[player][node.i_set][i]
                self.avg_strat_at_node(child, new_player_reach, new_player_average_reach)
            if (player, node.i_set) not in self.avg_visited:
                self.avg_visited.append((player, node.i_set))
                self.average_in_is(player, node.i_set, player_reach[player], player_average_reach[player])

    def average_in_is(self, player, iset, reach, avg_reach):
        if reach + avg_reach == 0:
            self.set_uniform_strategy(player, iset, self.average_strategy)
        else:
            if self.progressive_strategy:
                # self.average_strategy[player][iset] = np.divide(
                #     (np.multiply(self.strategy[player][iset], reach * self.iteration * self.iteration)) + np.multiply(
                #         self.average_strategy[player][iset], avg_reach * self.iter_count),
                #     reach * self.iteration * self.iteration + avg_reach * self.iter_count)
                self.average_strategy[player][iset] = np.divide(
                    (np.multiply(self.strategy[player][iset], reach * self.iteration)) + np.multiply(
                        self.average_strategy[player][iset], avg_reach * self.iter_count),
                    reach * self.iteration + avg_reach * self.iter_count)
            else:
                self.average_strategy[player][iset] = np.divide(
                    np.multiply(self.strategy[player][iset], reach) + np.multiply(
                        self.average_strategy[player][iset], avg_reach * self.iteration),
                    reach + avg_reach * self.iteration)

    # ------------------------- INITIALIZATION PART -------------------------
    # constructing the cfr
    def create_isets(self):
        # create empty array for isets in trunk for both players
        self.put_to_isets(self.game.root, depth=0)

    def put_to_isets(self, node, depth):
        # gather variables from the node for better understanding
        player = node.player
        if player > 2:
            return
        if player == 2:
            for child in node.children:
                self.put_to_isets(child, depth=depth + 1)
        else:
            iset = node.i_set
            # put node to iset and create new iset list if it does not exist yet
            if iset not in self.i_sets_to_nodes[player]:
                self.i_sets_to_nodes[player][iset] = []
            self.i_sets_to_nodes[player][iset].append(node)
            # put iset to subgame
            if iset not in self.i_sets[player]:
                self.i_sets[player].append(iset)
            for child in node.children:
                self.put_to_isets(child, depth=depth + 1)

    # strategy initialization
    def initialize_random_strategy(self):
        self.initialize_strategy_randomly(player=1)
        self.initialize_strategy_randomly(player=0)
        self.average_strategy = copy.deepcopy(self.strategy)

    def initialize_strategy_randomly(self, player):
        for iset in self.i_sets[player]:
            self.set_random_strategy(player=player, iset=iset, strategy=self.strategy)

    def set_random_strategy(self, player, iset, strategy):
        sample_node = self.i_sets_to_nodes[player][iset][0]
        n_children = len(sample_node.children)
        strat = np.random.rand(n_children)
        strategy[player][iset] = strat / np.sum(strat)

    def initialize_strategy(self):
        self.initialize_strategy_uniformly(player=1)
        self.initialize_strategy_uniformly(player=0)
        self.average_strategy = copy.deepcopy(self.strategy)
        if self.fixed is not None:
            for player in range(2):
                for iset in self.fixed[player]:
                    self.strategy[player][iset] = self.fixed[player][iset]

    def initialize_strategy_uniformly(self, player):
        for iset in self.i_sets[player]:
            self.set_uniform_strategy(player=player, iset=iset, strategy=self.strategy)

    def set_uniform_strategy(self, player, iset, strategy):
        sample_node = self.i_sets_to_nodes[player][iset][0]
        n_children = len(sample_node.children)
        strategy[player][iset] = [x / n_children for x in [1] * n_children]

    # regret initialization
    def initialize_regrets(self):
        self.initialize_regret_for_player(0)
        self.initialize_regret_for_player(1)

    def initialize_regret_for_player(self, player):
        for iset in self.i_sets[player]:
            self.counterfactual_regret[player][iset] = [0] * len(self.i_sets_to_nodes[player][iset][0].children)

    # create iset tree
    def create_iset_tree(self):
        self.create_iset_tree_step(self.game.root, (-1, -1), (0, 0))

    def create_iset_tree_step(self, node, isets, actions):
        player = node.player
        if player == 3:
            for player_index in range(2):
                if isets[player_index] not in self.iset_tree[player_index]:
                    self.iset_tree[player_index][isets[player_index]] = {}
                if actions[player_index] not in self.iset_tree[player_index][isets[player_index]]:
                    self.iset_tree[player_index][isets[player_index]][actions[player_index]] = set()
                self.iset_tree[player_index][isets[player_index]][actions[player_index]].add(node)
            return
        elif player == 2:
            for child in node.children:
                self.create_iset_tree_step(child, isets, actions)
        else:
            new_isets = [isets[0], isets[1]]
            new_isets[player] = node.i_set
            new_isets = tuple(new_isets)
            if isets[player] not in self.iset_tree[player]:
                self.iset_tree[player][isets[player]] = {}
            if actions[player] not in self.iset_tree[player][isets[player]]:
                self.iset_tree[player][isets[player]][actions[player]] = set()
            self.iset_tree[player][isets[player]][actions[player]].add(new_isets[player])
            for i, child in enumerate(node.children):
                new_actions = [actions[0], actions[1]]
                new_actions[player] = i
                new_actions = tuple(new_actions)
                self.create_iset_tree_step(child, new_isets, new_actions)

    # ------------------------- RESPONSES PART -------------------------
    # abstract response
    def general_response(self, strategy, player, response_function, *args):
        backup_strategy = self.strategy
        self.strategy = copy.deepcopy(strategy)
        self.compute_reaches(player, self.game.root, 1)
        self.compute_response_values_top(player, response_function, *args)
        return_strategy = self.strategy
        self.compute_game_value(self.strategy)
        self.strategy = backup_strategy
        return self.game_value, return_strategy

    def compute_reaches(self, player, node, reach):
        self.response_reaches[node] = reach
        if node.player == 3:
            return
        elif node.player == 2:
            for chance, child in zip(node.chance, node.children):
                self.compute_reaches(player, child, reach * chance)
        elif node.player == player:
            for child in node.children:
                self.compute_reaches(player, child, reach)
        else:
            for i, child in enumerate(node.children):
                self.compute_reaches(player, child, reach * self.strategy[1 - player][node.i_set][i])

    def compute_response_values_top(self, player, response_function, *args):
        self.compute_response_values_step(player, response_function, -1, *args)

    def compute_response_values_step(self, player, response_function, iset, *args):
        action_values = [0] * len(self.iset_tree[player][iset])
        for action in self.iset_tree[player][iset]:
            for node_or_set in self.iset_tree[player][iset][action]:
                if isinstance(node_or_set, int):
                    action_values[action] += self.compute_response_values_step(player, response_function, node_or_set,
                                                                               *args)
                else:
                    action_values[action] += (1 if player == 0 else -1) * node_or_set.value * self.response_reaches[
                        node_or_set]
        if self.fixed is None or iset not in self.fixed[player]:
            self.strategy[player][iset] = response_function(action_values, player, iset, *args)
        return np.sum(np.multiply(self.strategy[player][iset], action_values))

    def compute_strategy_in_set_with_function(self, player, response_function, iset):
        self.strategy[player][iset] = response_function(player, iset)

    # quantal response
    def quantal_response(self, player, strategy, rationality):
        return self.general_response(strategy, player, self.quantal_response_function, rationality)

    @staticmethod
    def quantal_response_function(values, player, iset, rationality=1):
        values = rationality * np.asarray(values)
        exped = np.exp(values - np.max(values))
        return exped / sum(exped)

    # quantal response
    def normalized_quantal_response(self, player, strategy, rationality):
        return self.general_response(strategy, player, self.normalized_quantal_response_function, rationality)

    def normalized_quantal_response_function(self, values, player, iset, rationality=1):
        reach = 1
        if iset != -1:
            reach = np.sum([self.response_reaches[x] for x in self.i_sets_to_nodes[player][iset]])
            if reach == 0:
                # works with average strategy
                return self.average_strategy[player][iset]
                # reach = 1
        values = rationality * (np.divide(values, reach))
        exped = np.exp(values - np.max(values))
        return exped / sum(exped)

    # best_response
    def best_response(self, player, strategy):
        return self.general_response(strategy, player, self.best_response_function)

    @staticmethod
    def best_response_function(values, player, iset):
        strategy_from_values = [0] * len(values)
        strategy_from_values[np.argmax(values)] = 1
        return strategy_from_values

    # linear quantal response
    def linear_response(self, player, strategy, rationality):
        return self.general_response(strategy, player, self.linear_response_function, rationality)

    @staticmethod
    def linear_response_function(values, player, iset, rationality):
        rationality = rationality / len(values)
        valsum = np.sum(values)
        if len(values) == 1:
            return [1]
        else:
            valmin = np.min(values)
            if rationality == 0:
                constant = 0
            else:
                numval = len(values)
                constant = (valsum - numval * valmin) / (1 / rationality - numval)
            values = values - valmin + constant
            valsum = np.sum(values)
            if valsum == 0:
                values += 1
            valsum = np.sum(values)
            return values / valsum

    # ------------------------- PRINT PART -------------------------

    @staticmethod
    def print_strategy(strategy, player, compact=False, decimal_points=4):
        for iset, values in strategy[player].items():
            if iset == -1:
                continue
            print(iset, end=": ")
            for val in values:
                print("{:.{dec}f}".format(val, dec=decimal_points), end=" ")
            if not compact:
                print()
        print()

    # prints counterfactual values in information sets
    def print_couterfactual_values(self):
        print("Player 2")
        for iset in self.i_sets[1]:
            cfv = [0] * len(self.i_sets_to_nodes[1][iset][0].children)
            for node in self.i_sets_to_nodes[1][iset]:
                for i, child in enumerate(node.children):
                    print(self.counterfactual_values[child][1])
                    cfv[i] += self.counterfactual_values[child][1]
            value = 0
            for i in range(len(cfv)):
                value += cfv[i] * self.strategy[1][iset][i]
            print(iset, cfv, value)
        print("Player 1")
        for iset in self.i_sets[0]:
            cfv = [0] * len(self.i_sets_to_nodes[0][iset][0].children)
            for node in self.i_sets_to_nodes[0][iset]:
                for i, child in enumerate(node.children):
                    print(self.counterfactual_values[child][0])
                    cfv[i] += self.counterfactual_values[child][0]
            value = 0
            for i in range(len(cfv)):
                value += cfv[i] * self.strategy[0][iset][i]
            print(iset, cfv, value)

    # ------------------------- COMPUTE METRICS PART -------------------------
    def sum_of_strategy_regret(self):
        self.compute_counterfactual_values()
        regret_sum = 0
        player = 1
        for iset in self.i_sets[player]:
            cfv = [0] * len(self.i_sets_to_nodes[player][iset][0].children)
            for node in self.i_sets_to_nodes[player][iset]:
                for i, child in enumerate(node.children):
                    cfv[i] += self.counterfactual_values[child][player]
            value = 0
            for i in range(len(cfv)):
                value += cfv[i] * self.strategy[player][iset][i]
            for i, cfv_value in enumerate(cfv):
                regret_sum += max(cfv_value - value, 0)
        return regret_sum
