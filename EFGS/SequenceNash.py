from ExtensiveGame import ExtensiveGame
from CFR import CFR
from gurobipy import *


class SequenceNash:
    def __init__(self, fname):
        # save game name
        self.fname = fname

        # Game variables
        self.game = ExtensiveGame()
        self.game.load(fname)

        # Variables to save
        self.is_to_node = {}
        self.action_index = {}
        self.action = 1
        self.i_set = 1
        self.i_sets = set()

        # Constraints
        self.x = None
        self.obj = None
        self.u = None
        self.m = None

        self.u_exp = None
        self.obj_exp = None
        # Just information variable
        self.nodes = 0

        # Saved solution in object
        self.solution = None

    def create_variables_public(self):
        self.create_variables(self.game.root, (-1, -1), 0, 1.0)

    def solve(self):
        self.nodes = 0
        self.create_variables(self.game.root, (-1, -1), 0, 1.0)
        self.solution = self.create_expresions()
        return self.solution  # print(res)

    def create_variables(self, node, last_node_action, last_node_strat, chance):
        self.nodes += 1
        if node.player == 3:
            # Terminal node
            if last_node_action not in self.is_to_node:
                self.is_to_node[last_node_action] = []
            self.is_to_node[last_node_action].append((node, chance, last_node_strat))
        elif node.player == 2:
            # Chance node
            for child, next_chance in zip(node.children, node.chance):
                self.create_variables(child, last_node_action, last_node_strat, chance * next_chance)
        elif node.player == 0:
            # Player 1
            if last_node_action not in self.is_to_node:
                self.is_to_node[last_node_action] = []
            if node.i_set not in self.is_to_node[last_node_action]:
                self.is_to_node[last_node_action].append(node.i_set)
                self.i_sets.add(node.i_set)
            for i, child in enumerate(node.children):
                self.create_variables(child, (node.i_set, i), last_node_strat, chance)
        else:
            # Player 2 (my agent)
            create_strats = False
            if (last_node_strat, node.i_set) not in self.action_index:
                self.action_index[(last_node_strat, node.i_set)] = []
                create_strats = True
            for i, child in enumerate(node.children):
                if create_strats:
                    self.action_index[(last_node_strat, node.i_set)].append(self.action)
                    self.action += 1
                self.create_variables(child, last_node_action, self.action_index[((last_node_strat, node.i_set))][i],
                                      chance)

    def create_expresions(self):
        self.m = Model("lp")

        # variables
        self.x = self.m.addVars(self.action, lb=0, ub=1, vtype=GRB.CONTINUOUS)
        self.obj = self.m.addVar(lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS, name="objective")
        self.u = self.m.addVars(len(self.i_sets), lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS)
        self.m.update()
        self.m.setObjective(self.obj, GRB.MINIMIZE)

        self.m.addConstr(self.x[0] == 1)

        for key, value in self.action_index.items():
            expr = LinExpr()
            for val in value:
                expr.add(self.x[val])
            self.m.addConstr(expr == self.x[key[0]])

        for key, value in self.is_to_node.items():
            expr = LinExpr()
            for val in value:
                if isinstance(val, int):
                    expr.add(self.u[val])
                else:
                    expr.add(self.x[val[2]] * val[0].value * val[1])
            if key[0] == -1:
                self.m.addConstr(expr <= self.obj)
            else:
                self.m.addConstr(expr <= self.u[key[0]])

        self.m.setParam('OutputFlag', False)
        # self.m.write("lp.lp")
        self.m.optimize()

        return self.m.objVal

    def objective(self, x):
        # print(x[self.action])
        return x[self.action]

    def print_strategy(self):
        if self.solution is None:
            print("Instance not yet solved.")
        else:
            for key, value in self.action_index.items():
                print(str(key) + ":", end="")
                if self.x[key[0]].x < 0.000001:
                    print(" Parent strategy is 0", end="")
                else:
                    for val in value:
                        print(" " + str(self.x[val].x / self.x[key[0]].x), end="")
                print()

    def strategy_in_cfr_format(self):
        if self.solution is None:
            print("Instance not yet solved.")
        else:
            cfrqr = CFR(self.fname)
            cfrqr.create_isets()
            cfrqr.initialize_strategy()
            for key, value in self.action_index.items():
                temp = [0] * len(value)
                i = 0
                if self.x[key[0]].x < 0.000001:
                    temp = [1 / len(value)] * len(value)
                else:
                    for val in value:
                        temp[i] = self.x[val].x / self.x[key[0]].x
                        i += 1
                cfrqr.strategy[1][key[1]] = temp
            return cfrqr.strategy
