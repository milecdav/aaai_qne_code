/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague
	
    This file is part of Game Theoretic Library.
	
    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.
	
    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
	
    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.
	
    If not, see <http://www.gnu.org/licenses/>.
*/

#include "cfr_adapt.h"

#include <utility>

namespace GTLib2::algorithms {
    CFRADAPTAlgorithm::CFRADAPTAlgorithm(const Domain &domain, Player playingPlayer, CFRFData &cache,
                                         function<void(vector<double>, vector<double> &)> responseFunction,
                                         function<void(vector<double>, vector<double> &)> secondResponseFunction,
                                         double p, double step) :
            CFRFAlgorithm(domain, playingPlayer, cache, std::move(responseFunction)),
            secondResponseFunction_(move(secondResponseFunction)) {
        p_ = p;
        step_ = step;
    }

    CFRADAPTAlgorithm::CFRADAPTAlgorithm(const Domain &domain, Player playingPlayer, CFRFData &cache,
                                         function<void(vector<double>, vector<double> &)> responseFunction,
                                         function<void(vector<double>, vector<double> &)> secondResponseFunction)
            : CFRADAPTAlgorithm(domain, playingPlayer, cache, std::move(responseFunction),
                                std::move(secondResponseFunction), 0.5, 0.01) {}

    void CFRADAPTAlgorithm::fullAdaptIterationFixed() {
        runIteration(Player(playingPlayer_));
        updateInfosetRegrets(Player(playingPlayer_));

        double r = ((double) rand() / (RAND_MAX));
        double responseValue = 0;
        double secondResponseValue = 0;
        if (r >= p_) {
            computeReachProbabilities(opponent(playingPlayer_));
            responseValue = -response(responseFunction_);
            computeReachProbabilities(opponent(playingPlayer_));
            secondResponseValue = response(secondResponseFunction_, false);
        } else {
            computeReachProbabilities(opponent(playingPlayer_));
            secondResponseValue = response(secondResponseFunction_, false);
            computeReachProbabilities(opponent(playingPlayer_));
            responseValue = -response(responseFunction_);
        }

        std::ofstream outfile;

        outfile.open("log.txt", std::ios_base::app); // append instead of overwrite
        outfile << responseValue << ", " << secondResponseValue << "\n";

        outfile.close();
    }

    double CFRADAPTAlgorithm::fullAdaptIterationNotFixed(double decay, double previousValue) {
        runIteration(Player(playingPlayer_));
        updateInfosetRegrets(Player(playingPlayer_));

        double r = ((double) rand() / (RAND_MAX));
        double responseValue = 0;
        bool nextSecond = false;
        double secondResponseValue = 0;
        if (r >= p_) {
            computeReachProbabilities(opponent(playingPlayer_));
            responseValue = -response(responseFunction_);
            computeReachProbabilities(opponent(playingPlayer_));
            secondResponseValue = response(secondResponseFunction_, false);
            nextSecond = true;
        } else {
            computeReachProbabilities(opponent(playingPlayer_));
            secondResponseValue = response(secondResponseFunction_, false);
            computeReachProbabilities(opponent(playingPlayer_));
            responseValue = -response(responseFunction_);
        }

        std::ofstream outfile;

        outfile.open("log.txt", std::ios_base::app); // append instead of overwrite
        outfile << responseValue << ", " << secondResponseValue << "\n";

        outfile.close();

        double uppperThreshldValue = previousValue > 0 ? threshold_ * previousValue : previousValue / threshold_;
        double lowerThreshldValue = previousValue > 0 ? threshold_ / previousValue : previousValue * threshold_;

        if (responseValue > uppperThreshldValue) {
            if (previousSecond_) {
                p_ -= step_ - step_ * 2 * abs(p_ - 0.5);
            } else {
                p_ += step_ - step_ * 2 * abs(p_ - 0.5);
            }
        } else if (responseValue < lowerThreshldValue) {
            if (previousSecond_) {
                p_ += step_;
            } else {
                p_ -= step_;
            }
        }
        step_ *= decay;
        previousSecond_ = nextSecond;
        return responseValue;
    }


    void CFRADAPTAlgorithm::runAdaptIterations(int iterations, bool fixed, double decay) {
        if (!cache_.isCompletelyBuilt()) {
            cache_.buildTree();
        }

        double previousValue = 0;
        for (int i = 0; i < iterations; ++i) {
//            cout << i << "\n";
            if (fixed) {
                fullAdaptIterationFixed();
            } else {
                previousValue = fullAdaptIterationNotFixed(decay, previousValue);
            }

        }
    }

    void CFRADAPTAlgorithm::solveWithIterations(int iterations) {
        double decay = pow(2, -1. / iterations);
        runAdaptIterations(iterations, false, decay);
        cache_.resetRegrets();
//        bestUtility = domain_.getMaxUtility();
        runAdaptIterations(iterations, true, decay);
    }

    double CFRADAPTAlgorithm::getP() const {
        return p_;
    }
}