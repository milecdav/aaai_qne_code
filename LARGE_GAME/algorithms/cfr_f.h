/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague
	
    This file is part of Game Theoretic Library.
	
    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.
	
    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
	
    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.
	
    If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ALGORITHMS_CFR_F_H_
#define ALGORITHMS_CFR_F_H_

#include "base/algorithm.h"
#include "algorithms/cfr.h"

#include <fstream>

namespace GTLib2::algorithms {

/**
 * Container for regrets and average strategy accumulators
 */
    enum CFRFStrategySaving {
        Best, Current, Average
    };

    class CFRFData : public virtual InfosetCache,
                     public StrategyCache {

    public:
        inline explicit CFRFData(const Domain &domain, Player player) : CFRFData(domain, player, Best) {}

        inline explicit CFRFData(const Domain &domain, Player player, CFRFStrategySaving saving) :
                EFGCache(domain),
                InfosetCache(domain),
                cfrPlayer(player),
                saving(saving) {
            addCallback([&](const shared_ptr<EFGNode> &n) { this->createCFRInfosetData(n); });
            this->createCFRInfosetData(getRootNode());
        }

        inline CFRFData(const CFRFData &other) :
                EFGCache(other),
                InfosetCache(other),
                cfrPlayer(other.cfrPlayer) {
            addCallback([&](const shared_ptr<EFGNode> &n) { this->createCFRInfosetData(n); });
            infosetData = other.infosetData;
        }

        void reset() override {
            for (auto &[aoh, data] : infosetData) data.reset();
        }

        void resetRegrets() {
            for (auto &[aoh, data] : infosetData) data.resetRegrets();
        }

        inline void clear() override {
            InfosetCache::clear();
            infosetData.clear();
            this->createCFRInfosetData(getRootNode());
        }

        struct InfosetData {
            vector<double> regrets;
            vector<double> avgStratAccumulator;
            vector<double> regretUpdates;
            vector <vector<shared_ptr < AOH>>>
            childInfosets;
            vector <vector<shared_ptr < EFGNode>>>
            childTerminals;
            unsigned int numUpdates = 0;

            /**
             * Disable updating RM strategy in this infoset
             */
            bool fixRMStrategy = false;

            /**
             * Disable updating avg strategy accumulator in this infoset
             */
            bool fixAvgStrategy = false;

            explicit InfosetData(unsigned long numActions) {
                regrets = vector<double>(numActions, 0.0);
                avgStratAccumulator = vector<double>(numActions, 0.0);
                regretUpdates = vector<double>(numActions, 0.);
                childInfosets = vector<vector<shared_ptr<AOH>>>(numActions, vector<shared_ptr<AOH>>());
                childTerminals = vector<vector<shared_ptr<EFGNode>>>(numActions, vector<shared_ptr<EFGNode>>());
            }

            void resetRegrets() {
                std::fill(regrets.begin(), regrets.end(), 0.);
                std::fill(regretUpdates.begin(), regretUpdates.end(), 0.);
            }

            void resetAverageStrategy() {
                std::fill(avgStratAccumulator.begin(), avgStratAccumulator.end(), 0.);
                numUpdates = 0;
            }

            void reset() {
                resetAverageStrategy();
                resetRegrets();
            }
        };

        unordered_map <shared_ptr<AOH>, InfosetData> infosetData;
        vector <shared_ptr<AOH>> rootInfosets;
        Player cfrPlayer;
        CFRFStrategySaving saving;
        unordered_map<shared_ptr<EFGNode>, double> reachProbabilities;

        inline optional <ProbDistribution>
        strategyFor(const shared_ptr <AOH> &currentInfoset) const override {
            if (infosetData.find(currentInfoset) == infosetData.end()) return nullopt;
            return calcAvgProbs(infosetData.at(currentInfoset).avgStratAccumulator);
        }

    private:
        void createCFRInfosetData(const shared_ptr <EFGNode> &node) {
            if (node->type_ == TerminalNode) {
                reachProbabilities.emplace(node, 0.);
                putTerminalToParent(node, node);
            }

            if (node->type_ != PlayerNode) return;

            auto infoSet = node->getAOHInfSet();
            if (infosetData.find(infoSet) == infosetData.end()) {
                infosetData.emplace(make_pair(
                        infoSet, CFRFData::InfosetData(node->countAvailableActions())));
                if (node->type_ == PlayerNode && node->getPlayer() == opponent(cfrPlayer)) {
                    putInfosetToParent(node, infoSet);
                }
            }
        }

        void putInfosetToParent(const shared_ptr <EFGNode> &node, const shared_ptr <AOH> &infoSet) {
            shared_ptr<EFGNode> parent = std::const_pointer_cast<EFGNode>(node->getParent());
            if (!parent) {
                rootInfosets.push_back(infoSet);
            } else {
                if (parent->type_ == PlayerNode && parent->getPlayer() == opponent(cfrPlayer)) {
                    const auto &children = this->getChildrenFor(parent);
                    for (int i = 0; i < children.size(); i++) {
                        if (node == children[i]) {
                            infosetData.at(this->getInfosetFor(parent)).childInfosets[i].push_back(infoSet);
                        }
                    }
                } else {
                    putInfosetToParent(parent, infoSet);
                }
            }
        }

        void putTerminalToParent(const shared_ptr <EFGNode> &node, const shared_ptr <EFGNode> &terminal) {
            shared_ptr<EFGNode> parent = std::const_pointer_cast<EFGNode>(node->getParent());
            if (!parent) {
                return;
            } else {
                if (parent->type_ == PlayerNode && parent->getPlayer() == opponent(cfrPlayer)) {
                    const auto &children = this->getChildrenFor(parent);
                    for (int i = 0; i < children.size(); i++) {
                        if (node == children[i]) {
                            infosetData.at(this->getInfosetFor(parent)).childTerminals[i].push_back(terminal);
                        }
                    }
                } else {
                    putTerminalToParent(parent, terminal);
                }
            }
        }
    };


/**
 * Index of the chance (nature) player
 */
    constexpr int CFRF_CHANCE_PLAYER = 2;


/**
 * Run CFR on EFG tree for a number of iterations for both players.
 * This implementation is based on Algorithm 1 in M. Lanctot PhD thesis.
 */
    class CFRFAlgorithm : public GamePlayingAlgorithm {
    public:
        CFRFAlgorithm(const Domain &domain,
                      Player playingPlayer,
                      CFRFData &cache,
                      function<void(vector < double > , vector < double > &)> responseFunction);

        PlayControl runPlayIteration(const optional <shared_ptr<AOH>> &currentInfoset) override;

        optional <ProbDistribution> getPlayDistribution(const shared_ptr <AOH> &currentInfoset) override;

        inline double runIteration(const Player updatingPl) {
            return runIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, updatingPl);
        }

        /**
         * Run an updating iteration for specified player starting at some node.
         * @return counterfactual value for infoset under current (regret-matching) strategy.
         */
        double runIteration(const shared_ptr <EFGNode> &node, array<double, 3> reachProbs,
                            Player updatingPl);

        void runIterations(int numIterations);

        inline CFRFData &getCache() {
            return cache_;
        }

        // todo: move into private!
        void updateInfosetRegrets(Player updatingPl);

        void computeReachProbabilities(Player player);

        void computeAverageReachProbabilities(Player player);

        void fullIteration();

        double publicResponse(const function<void(vector < double > , vector < double > &)> &responseFunction);

    protected:
        CFRFData &cache_;
        function<void(vector < double > , vector < double > &)> responseFunction_;
        double bestUtility = domain_.getMaxUtility();
        bool utilityImproved = true;

        double response(const function<void(vector < double > , vector < double > &)> &responseFunction);

        double response(const function<void(vector < double > , vector < double > &)> &responseFunction, bool update);

    private:
        void
        computeReachProbabilities_(Player player, const shared_ptr <EFGNode> &node, double reachProb, bool current);

        double response_(const shared_ptr <AOH> &infoset,
                         const function<void(vector < double > , vector < double > &)> &responseFunction);

        double response();
    };

    function<void(vector < double > , vector < double > &)> createCfrBestResponseFunction();

    function<void(vector < double > , vector < double > &)> createCfrLogitQuantalResponseFunction(double lambda);

}  // namespace GTLib2

#endif  // ALGORITHMS_CFR_H_
