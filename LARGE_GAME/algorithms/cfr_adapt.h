/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague
	
    This file is part of Game Theoretic Library.
	
    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.
	
    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
	
    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.
	
    If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GTLIB_CFR_ADAPT_H
#define GTLIB_CFR_ADAPT_H

#include "base/algorithm.h"
#include "algorithms/cfr_f.h"
#include <fstream>

namespace GTLib2::algorithms {

    class CFRADAPTAlgorithm : public CFRFAlgorithm {
    public:
        CFRADAPTAlgorithm(const Domain &domain,
                          Player playingPlayer,
                          CFRFData &cache,
                          function<void(vector<double>, vector<double> &)> responseFunction,
                          function<void(vector<double>, vector<double> &)> secondResponseFunction);

        CFRADAPTAlgorithm(const Domain &domain, Player playingPlayer, CFRFData &cache,
                          function<void(vector<double>, vector<double> &)> responseFunction,
                          function<void(vector<double>, vector<double> &)> secondResponseFunction, double p,
                          double step);

        void solveWithIterations(int iterations);

        void runAdaptIterations(int iterations, bool fixed, double decay);

        double getP() const;

    protected:
        function<void(vector<double>, vector<double> &)> secondResponseFunction_;

        double p_;

    protected:
        double step_;

        void fullAdaptIterationFixed();

        double fullAdaptIterationNotFixed(double decay, double previousValue);

        double threshold_ = 1.001;
        bool previousSecond_ = false;
    };

}
#endif //GTLIB_CFR_ADAPT_H
