/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague
	
    This file is part of Game Theoretic Library.
	
    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.
	
    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
	
    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.
	
    If not, see <http://www.gnu.org/licenses/>.
*/

#include "algorithms/cfr_f.h"

#include <utility>

#include "algorithms/common.h"
#include "algorithms/utility.h"


namespace GTLib2::algorithms {

    CFRFAlgorithm::CFRFAlgorithm(const Domain &domain,
                                 Player playingPlayer,
                                 CFRFData &cache,
                                 function<void(vector<double>, vector<double> &)> responseFunction) :
            GamePlayingAlgorithm(domain, playingPlayer),
            cache_(cache),
            responseFunction_(std::move(responseFunction)) {}

    PlayControl CFRFAlgorithm::runPlayIteration(const optional<shared_ptr<AOH>> &currentInfoset) {
        if (currentInfoset == PLAY_FROM_ROOT && !cache_.isCompletelyBuilt()) cache_.buildTree();

        // the tree has been built before, we must have this infoset in memory
        assert(currentInfoset == PLAY_FROM_ROOT || !cache_.getNodesFor(*currentInfoset).empty());

        runIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, Player(0));
        updateInfosetRegrets(Player(0));

        runIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, Player(1));
        updateInfosetRegrets(Player(1));

        return ContinueImproving;
    }

    optional<ProbDistribution>
    CFRFAlgorithm::getPlayDistribution(const shared_ptr<AOH> &currentInfoset) {
        const auto &data = cache_.infosetData.at(currentInfoset);
        const auto &acc = data.avgStratAccumulator;
        return calcAvgProbs(acc);
    }

    void CFRFAlgorithm::updateInfosetRegrets(Player updatingPl) {
        for (auto&[aoh, data]:cache_.infosetData) {
            if (aoh->getPlayer() != updatingPl) continue;

            if (!data.fixAvgStrategy) {
                ++data.numUpdates;
            }

            if (!data.fixRMStrategy) {
                for (unsigned int i = 0; i < data.regrets.size(); ++i) {
                    data.regrets[i] += data.regretUpdates[i];
                    data.regretUpdates[i] = 0.0;
                }
            }
        }
    }

    double CFRFAlgorithm::publicResponse(const function<void(vector<double>, vector<double> &)> &responseFunction) {
        computeAverageReachProbabilities(opponent(playingPlayer_));
        return response(responseFunction);
    }

    void CFRFAlgorithm::fullIteration() {
        runIteration(Player(playingPlayer_));
        updateInfosetRegrets(Player(playingPlayer_));

        computeReachProbabilities(opponent(playingPlayer_));
        double responseValue = response(responseFunction_);

        std::ofstream outfile;

        outfile.open("log.txt", std::ios_base::app); // append instead of overwrite

        outfile << responseValue << "\n";

        outfile.close();
    }

    void CFRFAlgorithm::runIterations(int numIterations) {
        if (!cache_.isCompletelyBuilt()) {
            cache_.buildTree();
        }

        for (int i = 0; i < numIterations; ++i) {
//            cout << i << "\n";

            fullIteration();
        }
    }


    double CFRFAlgorithm::runIteration(const shared_ptr<EFGNode> &node,
                                       const array<double, 3> reachProbs,
                                       const Player updatingPl) {
        assert(cache_.isCompletelyBuilt());
        if (reachProbs[0] == 0 && reachProbs[1] == 0) return 0.0;
        if (node->type_ == TerminalNode) return node->getUtilities()[updatingPl];
        if (node->type_ == ChanceNode) {
            const auto &children = cache_.getChildrenFor(node);
            double ExpectedValue = 0.0;
            auto chanceProbs = node->chanceProbs();

            for (unsigned int i = 0; i != children.size(); i++) {
                array<double, 3> newReachProbs = {reachProbs[0],
                                                  reachProbs[1],
                                                  reachProbs[CFRF_CHANCE_PLAYER] * chanceProbs[i]};
                ExpectedValue += chanceProbs[i] * runIteration(children[i], newReachProbs, updatingPl);
            }
            return ExpectedValue;
        }

        assert(node->type_ == PlayerNode);
        const auto actingPl = node->getPlayer();
        const auto oppExploringPl = 1 - updatingPl;
        const auto &children = cache_.getChildrenFor(node);
        const auto &infoSet = cache_.getInfosetFor(node);
        const auto numActions = children.size();
        auto &infosetData = cache_.infosetData.at(infoSet);
        auto &reg = infosetData.regrets;
        auto &acc = infosetData.avgStratAccumulator;
        auto &regUpdates = infosetData.regretUpdates;
        auto rmProbs = calcRMProbs(reg);
        auto ChildrenActionValues = vector<double>(numActions, 0.0);
        double ExpectedValue = 0.0;

        for (unsigned int i = 0; i != children.size(); i++) {
            array<double, 3> newReachProbs = {reachProbs[0],
                                              reachProbs[1],
                                              reachProbs[CFRF_CHANCE_PLAYER]};
            newReachProbs[actingPl] *= rmProbs[i];

            ChildrenActionValues[i] += runIteration(children[i], newReachProbs, updatingPl);
            ExpectedValue += rmProbs[i] * ChildrenActionValues[i];
        }

        if (actingPl == updatingPl) {
            for (unsigned int i = 0; i < numActions; i++) {
                if (!infosetData.fixRMStrategy) {
                    double cfActionRegret = (ChildrenActionValues[i] - ExpectedValue)
                                            * reachProbs[oppExploringPl] * reachProbs[CFRF_CHANCE_PLAYER];
                    regUpdates[i] += cfActionRegret;
                }
                if (!infosetData.fixAvgStrategy) {
                    if (cache_.saving == Average) {
                        acc[i] += reachProbs[updatingPl] * rmProbs[i];
                    } else if (cache_.saving == Current) {
                        acc[i] = rmProbs[i];
                    } else {
                        if (utilityImproved) {
                            acc[i] = rmProbs[i];
                        }
                    }
                }
            }
        }
        return ExpectedValue;
    }

    void CFRFAlgorithm::computeReachProbabilities(Player player) {
        computeReachProbabilities_(player, cache_.getRootNode(), 1, true);
    }

    void CFRFAlgorithm::computeAverageReachProbabilities(Player player) {
        computeReachProbabilities_(player, cache_.getRootNode(), 1, false);
    }

    void CFRFAlgorithm::computeReachProbabilities_(Player player, const shared_ptr<EFGNode> &node, double reachProb,
                                                   bool current) {
        if (node->type_ == TerminalNode) {
            cache_.reachProbabilities.at(node) = reachProb;
            return;
        }

        if (node->type_ == ChanceNode) {
            const auto &children = cache_.getChildrenFor(node);
            auto chanceProbs = node->chanceProbs();

            for (unsigned int i = 0; i != children.size(); i++) {
                if (chanceProbs[i] == 0) continue;
                computeReachProbabilities_(player, children[i], reachProb * chanceProbs[i], current);
            }
            return;
        }

        if (node->type_ == PlayerNode) {
            const auto &children = cache_.getChildrenFor(node);
            if (node->getPlayer() == player) {
                for (unsigned int i = 0; i != children.size(); i++) {
                    computeReachProbabilities_(player, children[i], reachProb, current);
                }
            } else {
                const auto &infoSet = cache_.getInfosetFor(node);
                auto &infosetData = cache_.infosetData.at(infoSet);
                auto &reg = infosetData.regrets;
                if (!current) {
                    reg = infosetData.avgStratAccumulator;
                }
                auto rmProbs = calcRMProbs(reg);


                for (unsigned int i = 0; i != children.size(); i++) {
                    if (rmProbs[i] == 0) continue;
                    computeReachProbabilities_(player, children[i], reachProb * rmProbs[i], current);
                }
            }
        }
    }

    double
    CFRFAlgorithm::response(const function<void(vector<double>, vector<double> &)> &responseFunction, bool updateBest) {
        double value = 0.;
        for (const shared_ptr<AOH> &infoSet : cache_.rootInfosets) {
            value += response_(infoSet, responseFunction);
        }
        if (updateBest) {
            utilityImproved = false;
            if (value < bestUtility) {
                bestUtility = value;
                utilityImproved = true;
            }
        }
        return value;
    }

    double CFRFAlgorithm::response(const function<void(vector<double>, vector<double> &)> &responseFunction) {
        return response(responseFunction, true);
    }

    double CFRFAlgorithm::response_(const shared_ptr<AOH> &infoset,
                                    const function<void(vector<double>, vector<double> &)> &responseFunction) {
        auto &infosetData = cache_.infosetData.at(infoset);
        int numActions = infosetData.regrets.size();
        auto ChildrenActionValues = vector<double>(numActions, 0.0);
        for (int i = 0; i < numActions; i++) {
            for (const shared_ptr<AOH> &childInfoset : infosetData.childInfosets[i]) {
                ChildrenActionValues[i] += response_(childInfoset, responseFunction);
            }
            for (const shared_ptr<EFGNode> &terminalNode : infosetData.childTerminals[i]) {
                if (cache_.reachProbabilities[terminalNode] > 0) {
                    ChildrenActionValues[i] +=
                            terminalNode->getUtilities()[opponent(cache_.cfrPlayer)] *
                            cache_.reachProbabilities[terminalNode];
                    cache_.reachProbabilities[terminalNode] = 0;
                }
            }
        }

        responseFunction(ChildrenActionValues, infosetData.regrets);

        double returnValue = 0.;
        for (int i = 0; i < numActions; i++) {
            returnValue += infosetData.regrets[i] * ChildrenActionValues[i];
        }
        return returnValue;
    }

    // Response functions Please when adding response function add test for it
    function<void(vector<double>, vector<double> &)> createCfrBestResponseFunction() {
        return [](vector<double> actionValues, vector<double> &strategy) {
            int maxIndex = std::max_element(actionValues.begin(), actionValues.end()) - actionValues.begin();
            for (int i = 0; i < actionValues.size(); i++) {
                strategy[i] = 0;
            }
            strategy[maxIndex] = 1;
        };
    }

    function<void(vector<double>, vector<double> &)> createCfrLogitQuantalResponseFunction(double lambda) {
        return [lambda](vector<double> actionValues, vector<double> &strategy) {
            double maxValue = *std::max_element(actionValues.begin(), actionValues.end());
            vector<double> expValues(actionValues.size());
            double expSum = 0;
            for (int i = 0; i < actionValues.size(); i++) {
                expValues[i] = exp((actionValues[i] - maxValue) * lambda);
                expSum += expValues[i];
            }
            for (int i = 0; i < actionValues.size(); i++) {
                strategy[i] = expValues[i] / expSum;
            }
        };
    }
}  // namespace GTLib2
