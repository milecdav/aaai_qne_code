/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague
	
    This file is part of Game Theoretic Library.
	
    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.
	
    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
	
    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.
	
    If not, see <http://www.gnu.org/licenses/>.
*/

#include "base/base.h"

#include "algorithms/cfr_adapt.h"
#include "algorithms/stats.h"

#include "domains/goofSpiel.h"

using namespace GTLib2;


void cfrAdaptTest() {
    auto domain = domains::GoofSpielDomain::IIGS(4);

    int iterations = 1000;

    auto dataCFRADAPT = algorithms::CFRFData(*domain, Player(0), GTLib2::algorithms::Best);
    auto responseFunction = algorithms::createCfrLogitQuantalResponseFunction(1);
    auto secondResponseFunction = algorithms::createCfrBestResponseFunction();

    algorithms::CFRADAPTAlgorithm cfrAdapt(*domain, Player(0), dataCFRADAPT, responseFunction,
                                           secondResponseFunction, 0.5, 0.01);

    cfrAdapt.solveWithIterations(iterations);
    auto againstQR = cfrAdapt.publicResponse(responseFunction);
    auto againstBR = cfrAdapt.publicResponse(secondResponseFunction);
    cout << "againstQR: " << againstQR << ", ";
    cout << "againstBR: " << againstBR << "\n";
}

int main() {
    cfrAdaptTest();
}