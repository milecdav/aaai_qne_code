#ifndef GTLIB2_INCLUDES_H
#define GTLIB2_INCLUDES_H

#include <iostream>
#include <array>
#include <cassert>
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <limits>
#include <vector>
#include <iterator>
#include <random>
#include <sstream>
#include <stdlib.h>
#include <stdint.h>


namespace GTLib2 {

using std::array;
using std::cerr;
using std::cout;
using std::dynamic_pointer_cast;
using std::endl;
using std::function;
using std::isnan;
using std::isinf;
using std::make_pair;
using std::make_shared;
using std::make_unique;
using std::max;
using std::min;
using std::move;
using std::nullopt;
using std::optional;
using std::pair;
using std::reference_wrapper;
#define ref_wrap reference_wrapper
using std::shared_ptr;
using std::string;
using std::tie;
using std::to_string;
using std::tuple;
using std::unique_ptr;
using std::unordered_map;
using std::unordered_set;
using std::vector;

}

#endif //GTLIB2_INCLUDES_H
