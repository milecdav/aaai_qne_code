from CFR import CFR
import numpy as np
import random as rand


class CFRCOMB(CFR):

    def __init__(self, fname, rationality=1):
        super(CFRCOMB, self).__init__(fname)
        self.rationality = rationality
        self.p = 0.5
        self.thr = 1.00001
        self.decay = 1
        self.fixed = False
        self.move = 0.01
        self.last_br = True
        self.prev_val = 0

    def set_p(self, p):
        self.p = p

    def solve(self, iterations=1000, player=1):
        self.p = 0.5
        self.decay = np.power(2, -1 / iterations)
        self.init_alg()
        self.value_progression.append(
            (self.game.payoff_based_on_strategy_and_opponent_rationality(player, self.avg_strategy[player], 1),
             self.game.best_response_value(player, self.avg_strategy[player])))
        super().solve(iterations, player)
        self.avg_strategy = None
        self.init_alg()
        self.fixed = True
        return super().solve(iterations, player)

    def compute_opponent_strategy(self, iteration, player):
        qr_val = self.game.payoff_based_on_strategy_and_opponent_rationality(1, self.strategy[1], self.rationality)
        if not self.fixed:
            up_thr_val = self.thr * self.prev_val if self.prev_val > 0 else self.prev_val / self.thr
            do_thr_val = self.prev_val / self.thr if self.prev_val > 0 else self.thr * self.prev_val
            if qr_val > up_thr_val:
                if self.last_br:
                    self.p -= self.move
                else:
                    self.p += self.move
            elif qr_val < do_thr_val:
                if self.last_br:
                    self.p += self.move
                else:
                    self.p -= self.move
            self.prev_val = qr_val
            self.move *= self.decay
        r = rand.uniform(0, 1)
        if r < self.p:
            self.quantal_response(player)
            self.last_br = False
        else:
            self.cfr_strategy_update(iteration, player)
            self.last_br = True
        self.update_average_strategy(iteration, player)

    def quantal_response(self, player):
        action_values = self.game.my_action_values(player, self.strategy[1 - player])
        max_value = np.max(action_values)
        action_sum = np.sum(np.exp(self.rationality * (action_values - max_value)))
        for j in range(self.game.actions[player]):
            self.strategy[player][j] = np.exp(self.rationality * (action_values[j] - max_value)) / action_sum

    def best_response(self, player):
        action_values = self.game.my_action_values(player, self.strategy[1 - player])
        self.strategy[player] = [0] * self.game.actions[player]
        self.strategy[player][np.argmax(action_values)] = 1
