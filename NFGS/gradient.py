#!/usr/bin/env python3

from scipy import optimize
import numpy as np
from game import Game
from scipy.optimize import Bounds
from gurobipy import *
import sys
import time
from game import error_print


class Gradient:
    def __init__(self, fname, rationality):
        self.game = Game()
        self.game.load_from_file(fname)
        self.strategy = []
        self.rationality = rationality
        # print("Game loaded")
        # nash = self.get_nash()
        # # nash = np.ones(self.game.actions[0])*1/self.game.actions[0]
        # print("Nash done")
        # start = time.time()
        # sol = self.solve(nash)
        # end = time.time()
        # print(end - start)
        # print("Gradient done")
        # print("Nash strategy value", self.objective_func(nash))
        # print("Gradient descended value", sol[1])

    # def constraint(self, x):
    #     return {'type': 'eq',
    #       'fun' : lambda x: np.array([x - 1]),
    #       # 'jac' : lambda x: np.array([2.0, 1.0])
    #       }

    def solve_best_ne(self):
        bounds = [(0, 1) for x in range(self.game.actions[1])]
        ne_strat, ne_val = self.get_nash()
        upper = np.full(self.game.actions[0] + 1, ne_val)
        upper[0] = 1
        lower = np.full(self.game.actions[0] + 1, -np.inf)
        lower[0] = 1
        const = optimize.LinearConstraint(np.vstack((np.ones((1, self.game.actions[1])), self.game.payoffs[0])),
                                          lower, upper)
        return optimize.minimize(self.objective_func, ne_strat, method="SLSQP", bounds=bounds,
                                 constraints=const), self.objective_func(ne_strat)

    def solve(self, starting_point):
        bounds = [(0, 1) for x in range(self.game.actions[1])]
        # print("Optimization starting")
        const = optimize.LinearConstraint(np.ones(self.game.actions[1]), [1], [1])
        return optimize.minimize(self.objective_func, starting_point, method="SLSQP", bounds=bounds,
                                 constraints=const), self.objective_func(starting_point)
        # return optimize.fmin_slsqp(self.objective_func, starting_point, bounds=bounds, eqcons=[self.constraint],
        #                            full_output=True, iprint=0), self.objective_func(starting_point)

    def objective_func(self, strategy):
        if abs(np.sum(strategy) - 1) > 0.01:
            return -10
        return -np.matmul(np.matmul(strategy, np.transpose(self.game.payoffs[1])),
                          self.game.op_strategy(1, strategy, self.rationality))

    def random_strategy(self):
        actions = self.game.actions[1]
        strategy = np.random.uniform(0, 1, actions)
        strategy = strategy / np.sum(strategy)
        return strategy

    def pure_strategies(self):
        strategies = []
        for i in range(self.game.actions[1]):
            next_strategy = [0] * self.game.actions[1]
            next_strategy[i] = 1
            strategies.append(next_strategy)
        return strategies

    def get_nash(self):
        m = Model("lp")
        # variables
        x = m.addVars(range(self.game.actions[1]), lb=0, ub=1, vtype=GRB.CONTINUOUS)
        u = m.addVar(lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS, name="value")
        m.update()
        # objective
        m.setObjective(u, GRB.MINIMIZE)
        # constraints
        m.addConstr(x.sum() == 1)

        for i in range(self.game.actions[0]):
            expr = LinExpr()
            for j in range(self.game.actions[1]):
                expr.add(x[j] * self.game.payoffs[0][i][j])
            m.addConstr(expr <= u)

        m.setParam('OutputFlag', False)
        m.optimize()

        strategy = []

        for i in range(self.game.actions[1]):
            strategy.append(x[i].x)

        return np.asarray(strategy), m.objVal

    def get_nash_value(self):
        m = Model("lp")
        # variables
        x = m.addVars(range(self.game.actions[1]), lb=0, ub=1, vtype=GRB.CONTINUOUS)
        u = m.addVar(lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS, name="value")
        m.update()
        # objective
        m.setObjective(u, GRB.MINIMIZE)
        # constraints
        m.addConstr(x.sum() == 1)

        for i in range(self.game.actions[0]):
            expr = LinExpr()
            for j in range(self.game.actions[1]):
                expr.add(x[j] * self.game.payoffs[0][i][j])
            m.addConstr(expr <= u)

        m.setParam('OutputFlag', False)
        m.optimize()

        return m.objVal

    def get_general_sum_nash(self, strategy=True):
        m = Model("milp")
        x = [m.addVars(range(self.game.actions[0]), lb=0, ub=1, vtype=GRB.CONTINUOUS),
             m.addVars(range(self.game.actions[1]), lb=0, ub=1, vtype=GRB.CONTINUOUS)]
        u = [m.addVar(lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS, name="follower_value"),
             m.addVar(lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS, name="leader_value")]
        u_a = [m.addVars(range(self.game.actions[0]), lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS),
               m.addVars(range(self.game.actions[1]), lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS)]
        r = [m.addVars(range(self.game.actions[0]), lb=0, vtype=GRB.CONTINUOUS),
             m.addVars(range(self.game.actions[1]), lb=0, vtype=GRB.CONTINUOUS)]
        b = [m.addVars(range(self.game.actions[0]), vtype=GRB.BINARY),
             m.addVars(range(self.game.actions[1]), vtype=GRB.BINARY)]
        m.update()

        # objectives
        M = 20
        m.setObjective(u[1], GRB.MAXIMIZE)
        for i in range(2):
            m.addConstr(x[i].sum() == 1)
            for j in range(self.game.actions[i]):
                ua_expr = LinExpr()
                for k in range(self.game.actions[1 - i]):
                    if i == 0:
                        ua_expr.add(x[1 - i][k] * self.game.payoffs[i][j][k])
                    else:
                        ua_expr.add(x[1 - i][k] * self.game.payoffs[i][k][j])
                m.addConstr(ua_expr == u_a[i][j])
                m.addConstr(u[i] >= u_a[i][j])
                m.addConstr(r[i][j] == u[i] - u_a[i][j])
                m.addConstr(x[i][j] <= 1 - b[i][j])
                m.addConstr(r[i][j] <= M * b[i][j])
        m.setParam('OutputFlag', False)
        m.optimize()

        ret_strategy = []
        if strategy:
            for i in range(self.game.actions[1]):
                ret_strategy.append(x[1][i].x)
            return np.asarray(ret_strategy)
        else:
            return m.objVal

    def get_stackelberg_equilibrium(self, strategy=True):
        max_val = -np.inf
        best_strat = None
        for i in range(self.game.actions[0]):
            # model and variable creation
            m = Model("lp")
            x = m.addVars(range(self.game.actions[1]), lb=0, ub=1, vtype=GRB.CONTINUOUS)
            u = m.addVar(lb=-GRB.INFINITY, vtype=GRB.CONTINUOUS, name="leader_value")
            m.update()

            # constraints and objective
            m.setObjective(u, GRB.MAXIMIZE)
            m.addConstr(x.sum() == 1)
            expr = LinExpr()
            for j in range(self.game.actions[1]):
                expr.add(x[j] * self.game.payoffs[1][i][j])
            m.addConstr(u <= expr)
            for j in range(self.game.actions[0]):
                expr1 = LinExpr()
                expr2 = LinExpr()
                for k in range(self.game.actions[1]):
                    expr1.add(x[k] * self.game.payoffs[0][i][k])
                    expr2.add(x[k] * self.game.payoffs[0][j][k])
                m.addConstr(expr1 >= expr2)
            m.setParam('OutputFlag', False)
            # m.write("data/models/" + str(i) + "model.lp")
            m.optimize()

            if m.Status == 2:
                val = m.objVal
                ret_strategy = []
                for j in range(self.game.actions[1]):
                    ret_strategy.append(x[j].x)
                if val > max_val:
                    max_val = val
                    best_strat = ret_strategy
        if strategy:
            return best_strat
        else:
            return max_val
