import numpy as np
from game import Game


class CFR:
    def __init__(self, fname):
        self.game = Game()
        self.game.load_from_file(fname)
        self.strategy = []
        self.values = []
        self.regrets = []
        self.avg_strategy = []
        self.count = 0
        self.value_progression = []

    def init_alg(self):
        self.strategy = []
        self.values = []
        self.regrets = []
        self.avg_strategy = []
        self.count = 0
        for i in range(self.game.players):
            self.regrets.append([])
            self.values.append([])
            self.strategy.append([])
            self.avg_strategy.append([])
            uniform_value = 1 / self.game.actions[i]
            for j in range(self.game.actions[i]):
                self.strategy[i].append(uniform_value)
                self.avg_strategy[i].append(uniform_value)
                self.regrets[i].append(0)

    def compute_values(self):
        for i in range(self.game.players):
            self.values[i] = self.game.my_action_values(i, self.strategy[1 - i])

    def compute_regrets(self, player):
        for i in range(self.game.players):
            value = np.sum(np.asarray(self.values[i]) * np.asarray(self.strategy[i]))
            regrets = self.values[i] - value
            self.regrets[i] = (self.regrets[i] + regrets).clip(min=0)

    def set_uniform_strategy(self, player):
        uniform_value = 1 / self.game.actions[player]
        for j in range(self.game.actions[player]):
            self.strategy[player][j] = uniform_value

    def regret_matching(self, player, regret_sum):
        for j in range(self.game.actions[player]):
            self.strategy[player][j] = self.regrets[player][j] / regret_sum

    def update_average_strategy(self, iteration, player):
        self.avg_strategy[player] = np.asarray(self.avg_strategy[player])
        self.strategy[player] = np.asarray(self.strategy[player])
        # self.avg_strategy[player] = np.asarray(
        #     self.avg_strategy[player] * iteration + self.strategy[player]) / (iteration + 1)
        self.avg_strategy[player] = np.asarray(
            self.avg_strategy[player] * self.count + self.strategy[player] * iteration) / (self.count + iteration)

    def compute_player_strategy(self, iteration, player):
        self.cfr_strategy_update(iteration, player)

    def cfr_strategy_update(self, iteration, player):
        regret_sum = np.sum(self.regrets[player])
        if regret_sum == 0:
            self.set_uniform_strategy(player)
        else:
            self.regret_matching(player, regret_sum)
        self.update_average_strategy(iteration, player)

    def compute_opponent_strategy(self, iteration, player):
        self.cfr_strategy_update(iteration, player)

    def compute_next_strategy(self, iteration, player):
        self.compute_player_strategy(iteration, player)
        self.compute_opponent_strategy(iteration, 1 - player)

    def step(self, iteration, player):
        self.compute_values()
        # print(self.values)
        self.compute_regrets(player)
        # print(self.regrets)
        self.compute_next_strategy(iteration, player)
        # print(self.strategy)
        self.count += iteration

    def solve(self, iterations=1000, player=1):
        self.init_alg()
        self.value_progression.append(
            (self.game.payoff_based_on_strategy_and_opponent_rationality(1, self.avg_strategy[1], 1),
             self.game.best_response_value(1, self.avg_strategy[1])))
        for iteration in range(1, iterations + 1):
            self.step(iteration, player)
            # print(self.game.payoff_based_on_strategy_and_opponent_rationality(1, self.avg_strategy[1], 1))
            self.value_progression.append(
                (self.game.payoff_based_on_strategy_and_opponent_rationality(1, self.avg_strategy[1], 1),
                 self.game.best_response_value(1, self.avg_strategy[1])))
        # print(self.strategy)
        # print(self.avg_strategy)
        return self.avg_strategy[1], self.game.payoff_based_on_strategy_and_opponent_rationality(1,
                                                                                                 self.avg_strategy[1],
                                                                                                 1), self.game.best_response_value(
            1, self.avg_strategy[1])
