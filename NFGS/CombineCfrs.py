from game import Game
import numpy as np
from CFRCOMB import CFRCOMB


class Combine:
    def __init__(self, fname):
        self.fname = fname
        self.game = Game()
        self.game.load_from_file(fname)

    def compute_strategy(self, s1, s2, rationality):
        best_strategy = []
        best_value = -np.inf
        best_gamma = 0
        for gamma in np.linspace(0, 1, 101):
            strategy = s1 * gamma + s2 * (1 - gamma)
            val = self.game.payoff_based_on_strategy_and_opponent_rationality(1, strategy, rationality)
            if val > best_value:
                best_value = val
                best_gamma = gamma
                best_strategy = strategy
        return best_strategy, best_value, best_gamma

    def compute_whole_space(self, s1, s2, rationality):
        # print(s1, s2)
        strategies = []
        values = []
        br_values = []
        variables = 11
        for gamma in np.linspace(0, 1, variables):
            strategy = s1 * gamma + s2 * (1 - gamma)
            strategies.append(str(gamma))
            values.append(self.game.payoff_based_on_strategy_and_opponent_rationality(1, strategy, rationality))
            br_values.append(self.game.best_response_value(1, strategy))
        return list(np.linspace(0, 1, variables)), strategies, values, br_values

    def compute_combine_cfr_for_whole_space(self, rationality=1):
        strategies = []
        values = []
        br_values = []
        for gamma in np.linspace(0, 1, 11):
            cfrcomb = CFRCOMB(self.fname, rationality=1)
            variables = 11
            cfrcomb.set_p(gamma)
            sol = cfrcomb.solve(1000)
            strategy = sol[0]
            strategies.append(strategy)
            values.append(self.game.payoff_based_on_strategy_and_opponent_rationality(1, strategy, rationality))
            br_values.append(self.game.best_response_value(1, strategy))
        return list(np.linspace(0, 1, variables)), list(np.linspace(0, 1, variables)), values, br_values
