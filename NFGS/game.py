import numpy as np

import sys


def error_print(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


class Game:

    def __init__(self):
        self.players = 0
        self.payoffs = None
        self.actions = None

    def load_from_file(self, fname):
        with open(fname if fname.endswith(".gbt") else fname + '.gbt', 'r') as file:
            stage = 0
            player_next = 0
            actual_actions = []
            for line in file:
                if stage == 0:
                    if line.strip().endswith('}'):
                        stage = 1
                        start = line.find('{')
                        end = line.find('}')
                        players_string = line[start + 1:end].strip()
                        count = len([x for x in players_string if x == "\""]) / 2
                        self.players = int(count)
                        self.actions = np.empty(self.players, dtype=int)
                        actual_actions = np.zeros(self.players, dtype=int)
                if stage == 1:
                    if line.strip().startswith("{"):
                        while not line.startswith("\""):
                            # print(line)
                            line = line.strip()
                            line = line.strip("{").strip("}")
                        count = len([x for x in line if x == "\""]) / 2
                        self.actions[player_next] = count
                        player_next += 1
                    if line.startswith("}"):
                        shape = (self.players,) + tuple(self.actions)
                        self.payoffs = np.empty(shape, dtype=np.double)
                        stage = 2
                if stage == 2:
                    if line.startswith("{") and len(line.split()) > 4:
                        line = line.strip().strip("{").strip("}").strip().strip("\"").strip()
                        parts = line.split()
                        for player, part in enumerate(parts):
                            payoff = part.strip().strip(",").strip()
                            index = (player,) + tuple(actual_actions)
                            self.payoffs[index] = payoff
                        al = 0
                        while True:
                            if al >= len(actual_actions):
                                break
                            actual_actions[al] += 1
                            actual_actions[al] = actual_actions[al] % self.actions[al]
                            if actual_actions[al] == 0:
                                al += 1
                            else:
                                break

    def save_to_file(self, fname):
        if self.players == 0:
            error_print("Can not save game to file because it is not initialized!")
            return
        with open(fname + '.gbt', 'w') as file:
            with open("data/begining.txt", 'r') as start:
                for line in start:
                    file.write(line)
            file.write("\nNFG 1 R \"Untitled\" { ")
            for i in range(self.players):
                file.write("\"Player " + str(i + 1) + "\" ")
            file.write("}\n\n")
            file.write("{ ")
            for num_actions in self.actions:
                file.write("{ ")
                for i in range(num_actions):
                    file.write("\"" + str(i + 1) + "\" ")
                file.write("}\n")
            file.write("}\n\"\"\n\n{\n")
            actual_actions = np.zeros(self.players, dtype=int)
            end = False
            outcomes = 0
            while not end:
                outcomes += 1
                file.write("{ \"\" ")
                for player in range(self.players):
                    index = (player,) + tuple(actual_actions)
                    file.write(str(self.payoffs[index]))
                    file.write(" ")
                file.write("}\n")
                al = 0
                while True:
                    if al >= len(actual_actions):
                        end = True
                        break
                    actual_actions[al] += 1
                    actual_actions[al] = actual_actions[al] % self.actions[al]
                    if actual_actions[al] == 0:
                        al += 1
                    else:
                        break
            file.write("}\n")
            for i in range(outcomes):
                file.write(str(i + 1) + " ")
            file.write("\n")
            with open("data/end.txt", 'r') as end:
                for line in end:
                    file.write(line)

    def save_to_text_file(self, file_out, action1, action2):
        with open(file_out, "w") as file:
            file.write(str(self.actions[0]))
            file.write(" ")
            file.write(str(self.actions[1]))
            file.write(" ")
            file.write(str(action1))
            file.write(" ")
            file.write(str(action2))
            file.write("\n")
            for line in self.payoffs[0]:
                for val in line[:-1]:
                    file.write(str(val))
                    file.write(" ")
                file.write(str(line[-1]))
                file.write("\n")

    def print(self):
        if self.players == 0:
            error_print("Can not save game to file because it is not initialized!")
            return
        print("Players:", self.players)
        for player, actions in enumerate(self.actions):
            print("Player", player + 1, "has", actions, "actions")
        for player, payoffs in enumerate(self.payoffs):
            print("Player", player + 1, "payoffs:")
            print(payoffs)

    def op_strategy(self, player, strategy, rationality):
        if self.players == 0:
            error_print("Can not save game to file because it is not initialized!")
            return None
        if player < 0 or player >= self.players:
            error_print("Invalid value for player!")
            return None
        if self.players > 2:
            error_print("Sorry, so far works only for 2 player games!")
            return None
        if self.actions[player] != len(strategy):
            error_print("Strategy must be as long as is number of actions for the player!")
            return None
        if rationality < 0:
            error_print("Rationality must be at least 0")
            return None
        if abs(np.sum(strategy) - 1) > 0.01:
            error_print("Strategy must sum to 1!")
            return None
        opponent = 1 - player
        strategy = np.asarray(strategy)
        op_payoffs = None
        if player == 1:
            op_payoffs = self.payoffs[opponent] * strategy
        else:
            op_payoffs = self.payoffs[opponent] * strategy[:, np.newaxis]
        action_values = np.sum(op_payoffs, axis=player) * rationality
        max_val = np.max(action_values)
        exp_values = np.exp(action_values - max_val)
        count = np.sum(exp_values)
        op_strategy = exp_values / count
        return op_strategy

    def payoff_based_on_strategy_and_opponent_rationality(self, player, strategy, rationality):
        if self.players == 0:
            error_print("Can not save game to file because it is not initialized!")
            return None
        if player < 0 or player >= self.players:
            error_print("Invalid value for player!")
            return None
        if self.players > 2:
            error_print("Sorry, so far works only for 2 player games!")
            return None
        if self.actions[player] != len(strategy):
            error_print("Strategy must be as long as is number of actions for the player!")
            return None
        if rationality < 0:
            error_print("Rationality must be at least 0")
            return None
        if abs(np.sum(strategy) - 1) > 0.01:
            error_print("Strategy must sum to 1!")
            return None
        value_vector = self.action_values(player, strategy, rationality) * np.asarray(strategy)
        return np.sum(value_vector)

    def action_values(self, player, strategy, rationality):
        if self.players == 0:
            error_print("Can not save game to file because it is not initialized!")
            return None
        if player < 0 or player >= self.players:
            error_print("Invalid value for player!")
            return None
        if self.players > 2:
            error_print("Sorry, so far works only for 2 player games!")
            return None
        if self.actions[player] != len(strategy):
            error_print("Strategy must be as long as is number of actions for the player!")
            return None
        if rationality < 0:
            error_print("Rationality must be at least 0")
            return None
        if abs(np.sum(strategy) - 1) > 0.01:
            error_print("Strategy must sum to 1!")
            return None
        op_strategy = self.op_strategy(player, strategy, rationality)
        opponent = 1 - player
        my_payoffs = None
        if player == 1:
            my_payoffs = self.payoffs[player] * op_strategy[:, np.newaxis]
        else:
            my_payoffs = self.payoffs[player] * op_strategy
        return np.sum(my_payoffs, axis=opponent)

    def action_plot(self, player):
        op = 1 - player
        op_payoffs = self.payoffs[op]
        return op_payoffs

    def opponent_action_values(self, player, op_strategy):
        op_strategy = np.asarray(op_strategy)
        # print("Value")
        # print(op_strategy)
        opponent = 1 - player
        my_payoffs = None
        if player == 1:
            my_payoffs = self.payoffs[1 - player] * op_strategy[:, np.newaxis]
        else:
            my_payoffs = self.payoffs[1 - player] * op_strategy
        # print(my_payoffs)
        return np.sum(my_payoffs, axis=opponent)

    def my_action_values(self, player, op_strategy):
        op_strategy = np.asarray(op_strategy)
        # print("Value")
        # print(op_strategy)
        opponent = 1 - player
        my_payoffs = None
        if player == 1:
            my_payoffs = self.payoffs[player] * op_strategy[:, np.newaxis]
        else:
            my_payoffs = self.payoffs[player] * op_strategy
        # print(my_payoffs)
        return np.sum(my_payoffs, axis=opponent)

    def best_response_value(self, player, strategy, verbose=False):
        opponent_values = self.my_action_values(1 - player, strategy)
        my_values = self.opponent_action_values(1 - player, strategy)
        br_action_set = abs(opponent_values - np.max(opponent_values)) < 0.000001
        if verbose:
            print(opponent_values)
            print(my_values)
            print(br_action_set)
        return np.max(my_values[br_action_set])

    def transform_to_zero_sum(self):
        self.payoffs[1] = -self.payoffs[0]

    def normalize(self, low, high):
        min_val = np.min(self.payoffs[0])
        max_val = np.max(self.payoffs[0])
        self.payoffs[0] = (self.payoffs[0] - min_val) / (max_val - min_val) * (high - low) + low
        self.transform_to_zero_sum()
