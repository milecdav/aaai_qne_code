import numpy as np
from CFR import CFR
from CombineCfrs import Combine
from CFRQR import CFRQR
from gradient import Gradient
import argparse
from CFRCOMB import CFRCOMB
from CFR import CFR
import tabulate
import argparse
import tabulate

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='My example explanation')
    parser.add_argument(
        '-f',
        '--filename',
        default="data/zerosum/34x34/00001.gbt",
        help='Name of game file to be used. Format efg or gbt. (default is example game located in data/zerosum/34x34/00001.gbt)'
    )
    parser.add_argument(
        '-i',
        '--iterations',
        default=1000,
        help='Number of iterations for algorithm (default is 1000).',
        type=int
    )
    parser.add_argument(
        '-r',
        '--rationality',
        default=1,
        help='Rationality of the Quantal response opponent (default is 1).',
        type=float
    )
    parser.add_argument(
        '-g',
        '--gurobi',
        default=True,
        help='Is Gurobi available on your system (default is False)',
        type=bool
    )
    args = parser.parse_args()

    fname = args.filename
    iterations = args.iterations
    rationality = args.rationality
    gurobi = args.gurobi

    cfr = CFR(fname)
    cfr_strat = cfr.solve(iterations)[0]

    cfrqr = CFRQR(fname, rationality=rationality)
    cfrqr_strat = cfrqr.solve(iterations)[0]

    gradient = Gradient(fname, rationality)
    qse_strat = gradient.solve(gradient.random_strategy())[0]['x']

    combine = Combine(fname)
    comb_strat = combine.compute_strategy(cfr_strat, cfrqr_strat, rationality)[0]

    rqr = CFRCOMB(fname, rationality=rationality)
    rqr_strat = rqr.solve(iterations)[0]

    if gurobi:
        nash_strat = gradient.get_nash()
    print("Zero-sum efg performance")
    table = [["COMB:", cfr.game.payoff_based_on_strategy_and_opponent_rationality(1, comb_strat, 1),
              cfr.game.best_response_value(1, comb_strat)],
             ["GA:", cfr.game.payoff_based_on_strategy_and_opponent_rationality(1, qse_strat, 1),
              cfr.game.best_response_value(1, qse_strat)],
             ["RQR:", cfr.game.payoff_based_on_strategy_and_opponent_rationality(1, rqr_strat, 1),
              cfr.game.best_response_value(1, rqr_strat)],
             ["CFR-QR:", cfr.game.payoff_based_on_strategy_and_opponent_rationality(1, cfrqr_strat, 1),
              cfr.game.best_response_value(1, cfrqr_strat)],
             ["CFR:", cfr.game.payoff_based_on_strategy_and_opponent_rationality(1, cfr_strat, 1),
              cfr.game.best_response_value(1, cfr_strat)]]
    if gurobi:
        table.append(["NASH:", cfr.game.payoff_based_on_strategy_and_opponent_rationality(1, nash_strat, 1),
                      cfr.game.best_response_value(1, nash_strat)])
    print(tabulate.tabulate(table, headers=["Algortihm", "Utility vs QR", "Utility vs BR"]))
