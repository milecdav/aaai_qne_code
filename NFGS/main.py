from game import Game
from graph_builder import GraphBuilder
import numpy as np
from CFR import CFR
from QCFR import QCFR
from CFRQR import CFRQR
from gradient import Gradient
import GameGenerator
from glob import glob
import time
import os
import pickle
from DataAnalysis import *
from CombineCfrs import Combine
import time
from scipy.signal import argrelextrema
from AverageFunction import average_function
from CFRCOMB import CFRCOMB
import scipy.stats as st
import matplotlib.cm
from matplotlib.ticker import AutoMinorLocator
import matplotlib.font_manager


def cfrqr(fname):
    cfrqr = CFRQR(fname, 100)
    cfrqr.solve(1000000)
    print(cfrqr.strategy)


def tests():
    filenames = []
    results = np.empty((len(filenames), 2))
    for i, fname in enumerate(filenames):
        gradient = Gradient(fname.rstrip(".gbt"), 1)
        solution_g, nash_v = gradient.solve(gradient.get_nash())
        results[i, 0] = nash_v
        results[i, 1] = solution_g[1]
    np.savetxt("data/50x50/results.csv", results, delimiter=",")


def time_test_rqr(fname):
    print(fname)
    start = time.time()
    rqr = CFRCOMB(fname)
    for p in np.linspace(0, 1, 11):
        rqr.set_p(p)
        rqr.solve()
    end = time.time()
    print(end - start)


def start_points_tests(fname):
    # gradient = Gradient("data/testing/3x3/00448", 1)
    # solution_g, nash_v = gradient.solve([0.00910057, 0.78618677, 0.20471266])
    # return
    gradient = Gradient("data/testing/small/2x2/" + fname, 1)
    solution_g, nash_v = gradient.solve(gradient.get_nash())
    results = []
    for i in range(1000):
        solution, _ = gradient.solve(gradient.random_strategy())
        results.append(gradient.objective_func(solution['x']))
    print(gradient.objective_func(solution_g['x']))
    # print(solution_g[0])
    print(nash_v)
    print(results)


def time_test(dir):
    filenames = glob(dir + "/*")
    c_time = 0
    count = 0
    for i, fname in enumerate(filenames):
        gradient = Gradient(fname.replace('.gbt', ""), 1)
        start = time.time()
        solution_g, nash_v = gradient.solve(gradient.random_strategy())
        end = time.time()
        c_time += end - start
        count += 1
    print(c_time / count)
    # print(solution_g[0])
    # print(nash_v)
    # print(solution[1])


# filenames = glob("data/50x50/*")
# for fname in filenames:
#     print(fname)
#     gradient = Gradient(fname.rstrip(".gbt"), 1)
#     solution_g, nash_v = gradient.solve(gradient.get_nash())
#     results = []
#     for i in range(1000):
#         solution, _ = gradient.solve(gradient.random_strategy())
#         results.append(solution[1])
#     # print(solution_g[1])
#     # print(nash_v)
#     # print(results)
#     for res in results:
#         if abs(res - solution_g[1]) > 0.001:
#             print("Difference", res)
#             break
def generate_nonsymmetric_games(size_one=2, size_two=4):
    n_games_generated = 1000
    dir_name = str(size_one) + "x" + str(size_two)
    if not os.path.exists("data/testing/" + dir_name):
        os.makedirs("data/testing/" + dir_name)
    GameGenerator.generate_more("data/testing/" + dir_name + "/", n_games_generated, size=[size_one, size_two])


def generate_all_games():
    for game_size in [233, 377, 610, 987, 1597]:
        dir_name = str(game_size) + "x" + str(game_size)
        if not os.path.exists("data/testing/" + dir_name):
            os.makedirs("data/testing/" + dir_name)
        GameGenerator.generate_more("data/testing/" + dir_name + "/", 0, 1000,
                                    size=[game_size, game_size])


def results_from_pure():
    folders = glob("data/testing/8x8/")
    for folder in folders:
        print(folder)
        solution = []
        games = glob(folder + "*")
        for fname in games:
            gradient = Gradient(fname.rstrip('.gbt'), 1)
            for start in gradient.pure_strategies():
                solution.append(gradient.solve(start)[0])
        pickle_out = open("data/results/" + folder.lstrip("/data/testing\\").rstrip("\\").rstrip("/") + ".pure", "wb")
        pickle.dump(solution, pickle_out)
        pickle_out.close()


def generate_nash_values():
    folders = glob("data/testing/*x*/")
    for folder in folders:
        print(folder)
        solution = []
        games = glob(folder + "*")
        for fname in games:
            gradient = Gradient(fname.rstrip('.gbt'), 1)
            solution.append(gradient.game.best_response_value(1, gradient.get_nash()))
        pickle_out = open("data/results/" + folder.lstrip("/data/testing\\").rstrip("\\").rstrip("/") + ".nash", "wb")
        pickle.dump(solution, pickle_out)
        pickle_out.close()


def generate_qne_results():
    folders = glob("data/testing/*x*/")
    for folder in folders:
        print(folder)
        solution = []
        games = glob(folder + "*")
        for fname in games:
            print(fname)
            gradient = Gradient(fname.rstrip('.gbt'), 10)
            cfrqr = CFRQR(fname.rstrip('.gbt'), 10)
            x = cfrqr.solve(iterations=10000)
            solution.append((-gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, x, 10),
                             gradient.game.best_response_value(1, x)))
        pickle_out = open("data/results/" + folder.lstrip("/data/testing\\").rstrip("\\").rstrip("/") + ".qne10", "wb")
        pickle.dump(solution, pickle_out)
        pickle_out.close()


def generate_results():
    folders = glob("data/testing/*x*/")
    for folder in folders:
        print(folder)
        time_nash = []
        time_rand = []
        sol_nash = []
        sol_rand = []
        values = []
        games = glob(folder + "*")
        for fname in games:
            print(fname)
            gradient = Gradient(fname.rstrip('.gbt'), 10)
            start = time.time()
            solution_g, nash_v = gradient.solve(gradient.get_nash())
            sol_nash.append(solution_g)
            end = time.time()
            time_nash.append(end - start)
            time_tmp = []
            val_tmp = []
            sol_tmp = []
            for i in range(10):
                start = time.time()
                solution, _ = gradient.solve(gradient.random_strategy())
                val_tmp.append(gradient.objective_func(solution['x']))
                end = time.time()
                time_tmp.append(end - start)
                sol_tmp.append(solution)
            values.append((nash_v, gradient.objective_func(solution_g['x']), val_tmp))
            time_rand.append(time_tmp)
            sol_rand.append(sol_tmp)

        results = (time_nash, time_rand, sol_nash, sol_rand, values)
        pickle_out = open("data/results/" + folder.lstrip("/data/testing\\").rstrip("\\").rstrip("/") + ".plk10", "wb")
        pickle.dump(results, pickle_out)
        pickle_out.close()


def test_cfrqr_time(dir):
    # cfrqr = CFRQR("data/2000x2000", 1)
    # print("build")
    # start = time.time()
    # solution_g = cfrqr.solve()
    # end = time.time()
    # print(end - start)
    #
    # return
    folders = glob(dir + "/*")
    result_time = []
    for folder in folders:
        print(folder)
        count = 0
        c_time = 0
        filenames = glob(folder + "/*")
        for fname in filenames:
            cfrqr = CFRQR(fname.rstrip('.gbt'), 1)
            cfrbr = CFR(fname.rstrip('.gbt'))
            start = time.time()
            solution_g = cfrqr.solve(1000)
            solution_n = cfrbr.solve(1000)
            combine = Combine(fname.rstrip('.gbt'))
            solution = combine.compute_strategy(solution_g[0], solution_n[0], 1)
            end = time.time()
            count += 1
            c_time += end - start
        result_time.append((folder, c_time / count))
    pickle_out = open(dir + "results.time", "wb")
    pickle.dump(result_time, pickle_out)
    pickle_out.close()


def shift_scale_experiment():
    print("Normal")
    cfrqr = CFRQR("data/2ND")
    sol_cfrqr = cfrqr.solve()
    print("CFR-QR", sol_cfrqr, cfrqr.game.op_strategy(1, sol_cfrqr, 1))
    grad = Gradient("data/2ND", 1)
    sol_grad = grad.solve(grad.get_nash())[0]['x']
    print("Grad", sol_grad, grad.game.op_strategy(1, sol_grad, 1))
    print("Shift")
    cfrqr = CFRQR("data/2ND_shift")
    sol_cfrqr = cfrqr.solve()
    print("CFR-QR", sol_cfrqr, cfrqr.game.op_strategy(1, sol_cfrqr, 1))
    grad = Gradient("data/2ND_shift", 1)
    sol_grad = grad.solve(grad.get_nash())[0]['x']
    print("Grad", sol_grad, grad.game.op_strategy(1, sol_grad, 1))
    print("Scale")
    cfrqr = CFRQR("data/2ND_scale")
    sol_cfrqr = cfrqr.solve()
    print("CFR-QR", sol_cfrqr, cfrqr.game.op_strategy(1, sol_cfrqr, 1))
    grad = Gradient("data/2ND_scale", 1)
    sol_grad = grad.solve(grad.get_nash())[0]['x']
    print("Grad", sol_grad, grad.game.op_strategy(1, sol_grad, 1))


def create_prog():
    folders = glob("data/testing/144x144/*")
    new_progression = []
    for file in folders:
        print(file)
        cfrqr = CFRQR(file.replace(".gbt", ""))
        cfrqr.solve()
        new_progression.append(cfrqr.value_progression)
    pickle_out = open("data/results/br_progression.prog", "wb")
    pickle.dump(new_progression, pickle_out)
    pickle_out.close()


def load_and_print_part():
    fname = "data/results/br_progression.prog"
    pickle_in = open(fname, "rb")
    loaded_data = pickle.load(pickle_in)
    print(loaded_data[0])


def combine_cfr_and_cfrqr(folder):
    files = glob("data/3ND_FM.gbt")
    files = glob("data/testing/" + folder + "/*")
    lbl = []
    clbl = []
    cfr_res = []
    cfrqr_res = []
    combine_res = []
    i = 0
    for file in files:
        lbl.append(i)
        clbl.append(str(i))
        i += 1
        print(file)
        # strip gbt
        file = file.strip(".gbt")
        # solve cfr
        cfr = CFR(file)
        cfr_sol = cfr.solve()

        # solve cfrqr
        cfrqr = CFRQR(file)
        cfrqr_sol = cfrqr.solve()

        # Combination strategy
        combine = Combine(file)
        combine_sol = combine.compute_strategy(cfr_sol[0], cfrqr_sol[0], 1)
        print(cfr_sol[1], cfrqr_sol[1], combine_sol[1])
        cfr_res.append(cfr_sol[1])
        cfrqr_res.append(cfrqr_sol[1])
        combine_res.append(combine_sol[1])
    return (lbl, clbl, cfr_res, cfrqr_res, combine_res), ("", "", "ro", "go", "yo")


def save_to_file(save, fname):
    pickle_out = open(fname, "wb")
    pickle.dump(save, pickle_out)
    pickle_out.close()


def load_from_file(fname):
    pickle_in = open(fname, "rb")
    return pickle.load(pickle_in)


def average_with_labes(all_data):
    data = all_data[0]
    for i in range(2, len(data)):
        print(np.average(data[i]))


def exploitaility_vs_exploitation_dir(dir, rationality=1):
    fnames = glob("data/testing/" + dir + "/*")
    all_ret = []
    for fname in fnames:
        print(fname)
        ret = exploitability_vs_exploitation(fname, rationality)
        all_ret.append(ret)
    return all_ret


def exploitaility_vs_exploitation_all(rationality=1):
    fnames = glob("data/testing/*x*")
    for fname in fnames:
        save_to_file(exploitaility_vs_exploitation_dir(fname.replace("data/testing\\", ""), rationality=rationality),
                     "data/results/expl_vs_expl" + fname.replace("data/testing\\", "") + str(rationality) + ".eve")


def exploitability_vs_exploitation(fname, rationality=1, verbose=False):
    file = fname.strip(".gbt")

    # solve cfrqr
    if verbose:
        print("starting cfrqr")
    start = time.time()
    cfrqr = CFRQR(file, rationality=rationality)
    cfrqr_sol = cfrqr.solve()

    # solve qse
    if verbose:
        print("starting qse")
    gradient = Gradient(file, rationality)
    nash = gradient.get_nash()[0]
    # grad_sol = gradient.solve(nash)

    # Combination strategy
    if verbose:
        print("starting comb")
    combine = Combine(file)
    combine_results = combine.compute_whole_space(nash, cfrqr_sol[0], rationality)
    end = time.time()
    print("Comb: ", end - start)

    # gradient.solve(nash)
    # start = time.time()
    # print("QSE: ", start - end)
    # print(combine_results[2][0], combine_results[2][-1])
    # print(combine_results[3][0], combine_results[3][-1])

    return combine_results


def combination_data_for_graph_dir(dir):
    fnames = glob("data/testing/" + dir + "/*")
    all_ret = []
    for fname in fnames:
        print(fname)
        ret = combination_data_for_graph(fname)
        all_ret.append(ret)
    return all_ret, ("ro", "go", "yo", "bo", "rs", "gs", "ys", "bs"), (
        "COMB-QR", "QSE-QR", "NASH-QR", "RQR-QR", "COMB-BR", "QSE-BR", "NASH-BR", "RQR-BR")


def graph_from_comb_data(data, normalize=True, skip=1, QR=True, BR=True, sub_min=False, sub_max=False, x_labels=None,
                         errs=None, points=10):
    # print(len(data))
    input = data[0]
    flags = data[1]
    labels = data[2]
    if len(data) > 3:
        x_labels = data[3]
    # input_len = len(input[0])
    input_len = points
    # print(input)
    plt.rcParams.update({'font.size': 13, 'font.family': 'Times New Roman'})
    fig, ax = plt.subplots(figsize=(4, 3))
    plt.gcf().subplots_adjust(left=0.1, bottom=0.3)
    cmap = matplotlib.cm.get_cmap('CMRmap')
    if sub_min or sub_max:
        for j in range(len(input)):
            if sub_max:
                min_val = -np.inf
            else:
                min_val = np.inf
            for i in range(input_len):
                if i > points - 1:
                    continue
                if not QR and i < points / 2:
                    continue
                if not BR and i > points / 2 - 1:
                    continue
                if sub_max:
                    min_val = max(input[j][i], min_val)
                else:
                    if input[j][i] < min_val:
                        min_val = input[j][i]
            # print(input[j])
            min_val = input[j][2]
            temp = input[j]
            input[j] = []
            for i in range(input_len):
                if QR:
                    input[j].append((temp[i] - min_val))
                if BR:
                    input[j].append((-temp[i] + min_val))
    # print(input)
    if normalize:
        for j in range(len(input)):
            max_val = -np.inf
            min_val = np.inf
            for i in range(input_len):
                if i > points - 1:
                    continue
                if not QR and i < points / 2:
                    continue
                if not BR and i > points / 2 - 1:
                    continue
                if input[j][i] > max_val:
                    max_val = input[j][i]
                if input[j][i] < min_val:
                    min_val = input[j][i]
            if max_val - min_val > 0.000001:
                temp = input[j]
                input[j] = []
                for i in range(input_len):
                    input[j].append((temp[i] - min_val) / (max_val - min_val))
            else:
                input[j] = []
                for i in range(input_len):
                    input[j].append(0)
    reshaped = np.empty((input_len, len(input)))
    for i in range(len(input)):
        for j in range(input_len):
            reshaped[j][i] = input[i][j]
    # reshaped = input
    # print(len(data))
    if len(data) > 3:
        to_sort = [int(x[0:x.find('x')]) for x in data[3]]
        indexes = np.argsort(to_sort)
    # print(reshaped)
    x = np.arange(len(x_labels))
    ax.yaxis.set_minor_locator(AutoMinorLocator(4))
    ax.yaxis.grid(which='minor', alpha=0.5)
    # ax.set_yticks([0, 2, 4, 6, 8, 10])
    width = 0.8 / (points / 2)
    ax.set_xticks(x)
    ax.yaxis.grid(True, which='both')
    ax.set_axisbelow(True)
    ax.set_xticklabels([x_labels[x][0:x_labels[x].find('x')] for x in indexes])
    # ax.set_xticklabels(x_labels)
    if QR:
        pos = [0, 1, 4, 2, 3]
        # pos = [2, 0, 4, 5, 1, 3] # GS positions
    else:
        pos = [0, 1, 2, 2, 3]
        # pos = [2, 0, 4, 5, 1, 3] # GS positions
    sub = 0
    print(x)
    if BR:
        sub = int(points / 2)
    print(reshaped)
    for i in [0, 1, 3, 4, 2, 5, 6, 8, 9]:  # ZS games
        # for i in [1, 4, 0, 5, 2, 3, 7, 10, 6, 11, 8, 9]: # GS games
        if not QR and i < points / 2:
            continue
        if not BR and i > points / 2 - 1:
            continue
        print(i)
        # if BR and i == 7:
        #     continue
        # print(errs)
        # print(i)
        if len(data) <= 3:
            ax.bar(x=x + ((pos[i - sub] / (points / 2)) * 0.9 - 0.37), height=reshaped[i],
                   yerr=[x[i] for x in errs],
                   width=width, label=labels[i], color=cmap(flags[i] / 6 + 0.2), capsize=3)
        else:
            print(x + ((pos[i - sub] / (points / 2)) * 0.9 - 0.25))
            ax.bar(x=x + ((pos[i - sub] / (points / 2)) * 0.9 - 0.25), height=[reshaped[i][x] for x in indexes],
                   yerr=[errs[x][i] for x in indexes], width=width,
                   label=labels[i], color=cmap(flags[i] / 6 + 0.2))
            # ax.plot([data[3][x][0:data[3][x].find('x')] for x in indexes], [reshaped[i][x] for x in indexes], flags[i],
            #         label=labels[i].replace("-QR", "").replace("-BR", "").replace("QSE", "GA"), markersize=6)
    plt.xlabel("Game name")
    if BR:
        if QR:
            ax.legend(loc="best", shadow=True, fontsize='small')
            plt.ylabel("Expected value for player 1")
            plt.title("Performance comparison in NFGs")
        else:
            plt.gcf().subplots_adjust(bottom=0.18, left=0.15, right=0.99, top=0.88)
            ax.legend(loc=(0.01, 0.62), shadow=True, fontsize='small')
            plt.ylabel("Exploitability")
            plt.title("")
    else:
        if QR:
            plt.gcf().subplots_adjust(bottom=0.17, left=0.14, right=0.99, top=0.88)
            ax.legend(loc=(0.47, 0.02), shadow=True, fontsize='small')
            plt.ylabel("Expected utility")
            plt.title("")
        else:
            plt.ylabel("Nothing at all")
            plt.title("Nothing")
    for i, label in enumerate(ax.xaxis.get_ticklabels()):
        if i < 3:
            continue
        # if i % 2 == 0:
        #     label.set_visible(False)
    plt.legend(bbox_to_anchor=(0, 1.02, 1., .102), loc='lower left',
               ncol=6, mode="expand", borderaxespad=0., handlelength=0.4, handletextpad=0.1)
    plt.show()


def combination_data_for_graph(fname):
    file = fname.strip(".gbt")

    # solve cfrqr
    qne_time = time.time()
    cfrqr = CFRQR(file)
    cfrqr_sol = cfrqr.solve()
    qne_time = time.time() - qne_time

    # solve qse
    qse_time = time.time()
    gradient = Gradient(file, 1)
    nash = gradient.get_nash()[0]
    grad_sol = gradient.solve(nash)
    qse_time = time.time() - qse_time

    # Combination strategy
    comb_time = time.time()
    combine = Combine(file)
    combine_results = combine.compute_strategy(nash, cfrqr_sol[0], 1)
    comb_time = time.time() - comb_time

    # RQR strategy
    rqr_time = time.time()
    acc_qr = []
    acc_br = []
    for p in np.linspace(0, 1, 11):
        rqr = CFRCOMB(file)
        rqr.set_p(p)
        strategy, qr_val, br_val = rqr.solve()
        acc_qr.append(qr_val)
        acc_br.append(br_val)
    index = np.argmax(acc_qr)
    qr_val = acc_qr[index]
    br_val = acc_br[index]
    rqr_time = time.time() - rqr_time
    qne_br_val = acc_br[-1]
    qne_qr_val = acc_qr[-1]

    # return (combine_results[1], -grad_sol[0]['fun'],
    #         gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1), cfrqr_sol[
    #             1], gradient.game.best_response_value(1, combine_results[0]),
    #         gradient.game.best_response_value(1, grad_sol[0]['x']), gradient.game.best_response_value(1, nash),
    #         gradient.game.best_response_value(1, cfrqr_sol[0]))

    return (combine_results[1], -grad_sol[0]['fun'],
            gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1), qr_val, qne_qr_val,
            gradient.game.best_response_value(1, combine_results[0]),
            gradient.game.best_response_value(1, grad_sol[0]['x']), gradient.game.best_response_value(1, nash),
            br_val, qne_br_val, 2 * qne_time + comb_time, qse_time, rqr_time)


def create_qne_values_dir(dir):
    fnames = glob("data/testing/" + dir + "/*")
    all_ret = []
    for fname in fnames:
        print(fname)
        ret = create_qne_value_file(fname)
        all_ret.append(ret)
    return all_ret


def create_qne_value_file(fname):
    file = fname.strip(".gbt")

    # solve cfrqr
    qne_time = time.time()
    cfrqr = CFRQR(file)
    cfrqr_sol, qr_val, br_val = cfrqr.solve()
    qne_time = time.time() - qne_time
    return qr_val, br_val, qne_time


def can_i_get_to_qse_by_combination(fname):
    file = fname.strip(".gbt")

    # solve cfrqr
    cfrqr = CFRQR(file)
    cfrqr_sol = cfrqr.solve()

    # solve qse
    gradient = Gradient(file, 1)
    nash = gradient.get_nash()
    grad_sol = gradient.solve(nash)

    # Combination strategy
    combine = Combine(file)
    combine_results = combine.compute_strategy(nash, cfrqr_sol[0], 1)

    print(combine_results[1], -grad_sol[0]['fun'],
          gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1), cfrqr_sol[1])
    print(gradient.game.best_response_value(1, combine_results[0]),
          gradient.game.best_response_value(1, grad_sol[0]['x']),
          gradient.game.best_response_value(1, nash), gradient.game.best_response_value(1, cfrqr_sol[0]))

    if (abs(grad_sol[0]['fun'] + combine_results[1])) < 0.00001:
        print("Same value:")
        print(grad_sol[0]['fun'], -combine_results[1], strategy_to_string(grad_sol[0]['x']),
              strategy_to_string(combine_results[0]))
        return fname, grad_sol[0]['fun'], -combine_results[1], grad_sol[0]['x'], combine_results[0]


def can_i_get_to_qse_by_combination_bulk(dir):
    fnames = glob("data/testing/" + dir + "/*")
    all_ret = []
    for fname in fnames:
        print(fname)
        ret = can_i_get_to_qse_by_combination(fname)
        if ret is not None:
            all_ret.append(ret)
    return all_ret


def strategy_to_string(strategy):
    ret = "["
    for action in strategy:
        ret += "{:1.3f} ".format(action)
    ret.strip()
    ret += "]"
    return ret


def stats_on_combination_vs_qse(dir):
    data = load_from_file("data/results/qse_vs_comb_" + dir + ".qvc")
    n_files = len(glob("data/testing/" + dir + "/*"))
    print(len(data), n_files)


def can_i_get_to_qse_by_combination_all():
    fnames = glob("data/testing/*x*")
    for fname in fnames:
        save_to_file(can_i_get_to_qse_by_combination_bulk(fname.replace("data/testing\\", "")),
                     "data/results/qse_vs_comb_" + fname.replace("data/testing\\", "") + ".qvc")


def comb_data_for_graph_all_dirs(val):
    fnames = glob("data/testing/" + val + "x" + val)
    for fname in fnames:
        save_to_file(combination_data_for_graph_dir(fname.replace("data/testing/", "")),
                     "data/results/comb_data_for_graph_" + fname.replace("data/testing/", "") + ".qvc")


def add_game_value_to_expl_vs_exlp(dir):
    data = load_from_file("data/results/expl_vs_expl" + dir + ".eve")
    ret = []
    for line, fname in zip(data, glob("data/testing/" + dir + "/*")):
        print(fname)
        grad = Gradient(fname, rationality=1)
        nash_val = grad.get_nash_value()
        strat = grad.get_nash()
        nash_qr_val = grad.game.payoff_based_on_strategy_and_opponent_rationality(1, strat, 1)
        ret.append((line, nash_val, nash_qr_val))
    save_to_file(ret, "data/results/expl_vs_expl_with_value" + dir + ".evev")


def show_all_graphs_expl_vs_expl_dir_exlp_on_axis(dir):
    data = load_from_file("data/results/expl_vs_expl" + dir + ".eve")
    for line in data:
        new_line = []
        new_line.append([x - line[2][-1] for x in line[2]])
        new_line.append([-x + line[3][-1] for x in line[3]])

        plt.rcParams.update({'font.size': 16, 'font.family': 'Times New Roman'})
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.plot(new_line[1], new_line[0], label=str("r-"), markersize=12)
        plt.show()

        # my_graph((line[0], ("", "", "r-", "b-")), lines=False, hlines=True)


def show_all_graphs_expl_vs_expl_dir(dir):
    data = load_from_file("data/results/expl_vs_expl" + dir + ".eve")
    for line in data:
        line = (line[0], line[0], line[2], line[3])
        my_graph((line, ("", "", "r-", "b-")), lines=False, hlines=True)


def replace_data():
    fnames = glob("data/results/comb_data_backup/comb_data_for_graph_377x377.qvc")
    qr_index = 4
    br_index = 9
    for fname in fnames:
        data1 = load_from_file(fname)
        data2 = load_from_file(
            fname.replace("comb_data_backup/comb_data_for_graph", "qne/qne_values").replace("qvc", "qne"))
        data_ret = []
        data_ret.append([])
        for prev, repl in zip(data1[0], data2):
            temp = []
            for i in range(len(prev)):
                if i == qr_index:
                    temp.append(repl[0])
                    continue
                if i == br_index:
                    temp.append(repl[1])
                    continue
                else:
                    temp.append(prev[i])
            data_ret[0].append(temp)
        data_ret.append(data1[1])
        data_ret.append(data1[2])
        save_to_file(data_ret, "data/results/comb_data_for_graph_377x377.qvc")


def average_of_comb_data(loss):
    fnames = glob("data/results/comb_data_for_graph_*x*.qvc")
    avg_data = []
    fnames_ret = []
    flags = None
    labels = None
    stdevs_ret = []
    for fname in fnames:
        fnames_ret.append(fname.replace("data/results\\comb_data_for_graph_", "").replace(".qvc", ""))
        data = load_from_file(fname)
        avg_list = []
        for i in range(len(data[0][0])):
            avg_list.append([])
        for one_game in data[0]:
            for i in range(len(avg_list)):
                if loss:
                    avg_list[i].append(- one_game[i] + one_game[7])
                else:
                    avg_list[i].append(one_game[i] - one_game[7])
        stdevs = st.sem(avg_list, 1)
        for i in range(len(avg_list)):
            avg_list[i] = np.average(avg_list[i])
        # avg_list.pop(3)
        # avg_list.pop()
        avg_data.append(avg_list)
        stdevs_ret.append(stdevs)

    # flags = list(flags)
    # flags.pop(3)
    # flags.pop()
    # labels = list(labels)
    # labels.pop(3)
    # labels.pop()
    # avg_data[5] = np.asarray(avg_data[5])-0.1
    flags = [0, 1, 4, 2, 3, 0, 1, 4, 2, 3]
    labels = (
        "COMB", "GA", "NASH", "RQR", "RMQR", "COMB", "GA", "NASH", "RQR", "RMQR")
    return avg_data, flags, labels, fnames_ret, stdevs_ret


def zerosumification_test(fname):
    game = Game()
    game.load_from_file(fname)
    game.normalize(-10, 10)
    game.save_to_file(fname)


def compare_combinations(fname):
    combine = Combine(fname)
    data1 = combine.compute_combine_cfr_for_whole_space()
    cfrqr = CFRQR(fname)
    cfrqr_sol = cfrqr.solve()

    # solve qse
    gradient = Gradient(fname, 1)
    nash = gradient.get_nash()

    data2 = combine.compute_whole_space(nash, cfrqr_sol[0], 1)
    # my_graph((data2, ("", "", "r-", "b-")), lines=False, hlines=True)
    my_graph((data1 + data2[2:], ("", "", "r-", "b-", "r--", "b--")), lines=False, hlines=True)


def solve_best_ne(fname):
    gradient = Gradient(fname, rationality=1)
    solution = gradient.solve_best_ne()
    print(solution[0])
    print(solution[0]['fun'])
    print(solution[1])


def save_game_to_txt(file_in, file_out, action1=0, action2=0):
    game = Game()
    game.load_from_file(file_in)
    gradient = Gradient(fname=file_in, rationality=7)
    print(gradient.get_nash())
    game.save_to_text_file(file_out, action1, action2)


def compute_nash(fname):
    gradient = Gradient(fname, 1)
    strategy, value = gradient.get_nash()
    print(strategy)
    print(value)


def proces_test(dir):
    save_to_file(create_qne_values_dir(dir + "x" + dir), "data/results/qne/qne_values_" + dir + "x" + dir + ".qne")


def graph_from_times():
    fnames = glob("data/results/comb_data_for_graph_*x*.qvc")
    times = [[], [], []]
    fnames_ret = []
    plt.rcParams.update({'font.size': 13, 'font.family': 'Times New Roman'})
    for fname in fnames:
        fnames_ret.append(fname.replace("data/results\\comb_data_for_graph_", "").replace(".qvc", ""))
        file_times = [[], [], []]
        data = load_from_file(fname)
        for line in data[0]:
            for i in range(3):
                if i == 2:
                    file_times[i].append(line[i + 10] / 4)
                else:
                    file_times[i].append(line[i + 10])
        file_times = np.average(file_times, 1)
        for i in range(3):
            times[i].append(file_times[i])
    fig, ax = plt.subplots(figsize=(4, 3))
    to_sort = [int(x[0:x.find('x')]) for x in fnames_ret]
    indexes = np.argsort(to_sort)
    flags = ['b-', 'r--', 'g-.']
    labels = ['COMB', 'GA', 'RQR']
    plt.xlabel("Game size")
    plt.ylabel("Time [s]")
    plt.gcf().subplots_adjust(bottom=0.15, right=0.99, left=0.17, top=0.87)
    plt.yticks([0, 20, 40, 60, 80, 100])
    cmap = matplotlib.cm.get_cmap('CMRmap')
    ax.set_axisbelow(True)
    ax.yaxis.set_minor_locator(AutoMinorLocator(4))
    ax.xaxis.set_minor_locator(AutoMinorLocator(4))
    ax.yaxis.grid(which='minor', alpha=0.5)
    ax.xaxis.grid(which='minor', alpha=0.5)
    ax.yaxis.grid(True)
    ax.xaxis.grid(True)
    print(times)
    for i, time in enumerate(times):
        ax.plot([int(fnames_ret[x][0:fnames_ret[x].find('x')]) for x in indexes], [time[x] for x in indexes],
                color=cmap(i / 6 + 0.2), linestyle=flags[i][1:],
                label=labels[i], markersize=6)
    plt.legend(bbox_to_anchor=(0, 1.02, 1., .102), loc='lower left',
               ncol=3, mode="expand", borderaxespad=0., handlelength=1)
    plt.show()


def general_sum_test(fname):
    combine = Combine(fname)
    data1 = combine.compute_combine_cfr_for_whole_space()
    cfrqr = CFRQR(fname)
    cfrqr_sol = cfrqr.solve()
    # print(cfrqr_sol)

    # solve qse
    gradient = Gradient(fname, 1)
    nash = gradient.get_general_sum_nash(strategy=True)

    nash_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1)
    nash_br = gradient.game.best_response_value(1, nash)

    stackelberg = gradient.get_stackelberg_equilibrium(strategy=True)

    stackelberg_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, stackelberg, 1)
    stackelberg_br = gradient.game.best_response_value(1, stackelberg)

    grad_sol = gradient.solve(nash)[0]
    grad_qr = -grad_sol['fun']
    grad_strat = grad_sol['x']
    grad_br = gradient.game.best_response_value(1, grad_strat)

    data2 = combine.compute_whole_space(cfrqr_sol[0], nash, 1)
    # my_graph((data2, ("", "", "r-", "b-")), lines=False, hlines=True)
    my_graph((data1 + data2[2:], ("", "", "r--", "b--", "r-", "b-")), lines=False, hlines=True,
             label=["RQRQR", "RQRBR", "COMBQR", "COMBBR"],
             line_data=[(nash_qr, "NASHQR", "r", "-."), (nash_br, "NASHBR", "b", "-."),
                        (stackelberg_qr, "STACKQR", "r", ":"), (stackelberg_br, "STACKBR", "b", ":"),
                        (grad_qr, "GAQR", "m", ":"), (grad_br, "GABR", "c", ":")])


def general_sum_data(fname):
    start = time.time()
    combine = Combine(fname)
    data1 = combine.compute_combine_cfr_for_whole_space()
    end = time.time()
    print("RQR", end - start)
    start = time.time()
    cfrqr = CFRQR(fname)
    cfrqr_sol = cfrqr.solve()
    end = time.time()
    print("CFR-QR", end - start)
    # print(cfrqr_sol)

    # solve qse
    start = time.time()
    gradient = Gradient(fname, 1)
    nash = gradient.get_general_sum_nash(strategy=True)
    end = time.time()
    print("NE", end - start)

    nash_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1)
    nash_br = gradient.game.best_response_value(1, nash)

    start = time.time()
    stackelberg = gradient.get_stackelberg_equilibrium(strategy=True)
    end = time.time()
    print("SE", end - start)

    stackelberg_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, stackelberg, 1)
    stackelberg_br = gradient.game.best_response_value(1, stackelberg)

    start = time.time()
    grad_sol = gradient.solve(gradient.random_strategy())[0]
    grad_qr = -grad_sol['fun']
    grad_strat = grad_sol['x']
    grad_br = gradient.game.best_response_value(1, grad_strat)
    end = time.time()
    print("GA", end - start)

    start = time.time()
    data2 = combine.compute_whole_space(cfrqr_sol[0], nash, 1)
    end = time.time()
    print("COMB", end - start)

    return data1, data2, nash_qr, nash_br, stackelberg_qr, stackelberg_br, grad_qr, grad_br


def convert_nfg_to_gbt(fname_in, fname_out):
    stage = 0
    lines_out = []
    actions_one = 0
    actions_two = 0
    with open(fname_in, "r") as file:
        for line in file:
            if stage == 0:
                if line.strip().endswith("}"):
                    splits = line.split()
                    next_append = ""
                    for i in range(6):
                        next_append += splits[i] + " "
                    next_append += "\n"
                    lines_out.append(next_append)
                    actions_one = int(splits[7])
                    actions_two = int(splits[8])
                    stage = 1
                    lop_one = "{ "
                    for i in range(actions_one):
                        lop_one += "\"" + str(i + 1) + "\" "
                    lop_one += "}"
                    lop_two = "{ "
                    for i in range(actions_two):
                        lop_two += "\"" + str(i + 1) + "\" "
                    lop_two += "}"
                    lines_out.append("{ " + lop_one + "\n" + lop_two + "\n}\n\"\"\n\n{")
                else:
                    lines_out.append(line)
            elif stage == 1:
                stage = 2
            else:
                splits = line.split()
                for i in range(0, len(splits), 2):
                    lines_out.append("{ \"\" " + splits[i] + " " + splits[i + 1] + " }\n")
                lines_out.append("}\n")
                outcome_numbers = ""
                for i in range(actions_one * actions_two):
                    outcome_numbers += str(i + 1) + " "
                lines_out.append(outcome_numbers)
    with open(fname_out, "w") as file:
        file.writelines(lines_out)
    os.remove(fname_in)


def save_general_sum_data_all():
    dir_names = glob("data/testing/general_sum/7opt")
    for dir in dir_names:
        acc = []
        for fname in glob(dir + "/*"):
            print(fname)
            acc.append(general_sum_data(fname))
            save_to_file(acc, dir.replace("testing", "results").rstrip("\\") + ".data")


def load_gs_data():
    avg_data = []
    fnames_ret = []
    stdevs_ret = []
    for fname in glob("data/results/general_sum/max_br/*.data"):
        fnames_ret.append(fname.replace("data/results/general_sum/max_br\\", "").replace(".data", ""))
        data = load_from_file(fname)
        avg_list = []
        for i in range(12):
            avg_list.append([])
        for one_game in data:
            avg_list[5].append(one_game[0][2][-1])
            avg_list[11].append(one_game[0][3][-1])
            for i in range(2):
                max_index = np.argmax(one_game[i][2])
                avg_list[i].append(one_game[i][2][max_index])
                avg_list[i + 6].append(one_game[i][3][max_index])
            index = 2
            for i in range(2, 8):
                avg_list[index].append(one_game[i])
                if index < 5:
                    index += 6
                else:
                    index -= 5
        stdevs = st.sem(avg_list, 1)
        for i in range(len(avg_list)):
            avg_list[i] = np.average(avg_list[i])
        avg_data.append(avg_list)
        stdevs_ret.append(stdevs)

    flags = [2, 0, 4, 4.5, 1, 3, 2, 0, 4, 4.5, 1, 3]
    labels = (
        "RQR", "COMB", "NASH", "SE", "GA", "RMQR", "RQR", "COMB", "NASH", "SE",
        "GA", "RMQR",)
    return avg_data, flags, labels, fnames_ret, stdevs_ret


def plot_strategy_progression(min, max, step, fname, iterations=1000):
    rationality = min
    plot_qne_x = []
    plot_qne_y = []
    plot_qse_x = []
    plot_qse_y = []
    while rationality < max:
        print(rationality)
        # compute QNE and QSE
        cfrqr = CFRQR(fname, rationality)
        cfrqr.solve(iterations=iterations)
        strategy = cfrqr.avg_strategy
        cfrqr_strategy = strategy[1]
        plot_qne_x.append(round(cfrqr_strategy[0], 6))
        plot_qne_y.append(round(cfrqr_strategy[1], 6))

        qse = Gradient(fname, rationality)
        qse_sol = qse.solve(qse.random_strategy())
        qse_strategy = qse_sol[0]['x']
        plot_qse_x.append(round(qse_strategy[0], 6))
        plot_qse_y.append(round(qse_strategy[1], 6))

        print(qse.game.payoff_based_on_strategy_and_opponent_rationality(1, qse_strategy, rationality))
        print(qse.game.payoff_based_on_strategy_and_opponent_rationality(1, cfrqr_strategy, rationality))

        print("QSE:", np.round(qse_strategy, 6))
        print("QNE:", np.round(cfrqr_strategy, 6))

        # after computation
        rationality *= step

    # plotting stuff
    plt.rcParams.update({'font.size': 13, 'font.family': 'Times New Roman'})
    plt.plot(plot_qne_x, plot_qne_y, "1-", markersize=7, linewidth=1, label="QNE strategies")
    plt.plot(plot_qse_x, plot_qse_y, "2-", markersize=7, linewidth=1, label="QSE strategies")
    plt.xlabel("Probability of action X")
    plt.ylabel("Probability of action Y")
    plt.legend(bbox_to_anchor=(0, 1.02, 1., .102), loc='lower left',
               ncol=2, mode="expand", borderaxespad=0., handlelength=1)
    plt.grid(True)
    plt.minorticks_on()
    plt.grid(which='minor', alpha=0.5)
    plt.show()


def general_sum_time(fname, skip, big, med):
    start = time.time()
    combine = Combine(fname)
    combine.compute_combine_cfr_for_whole_space()
    end = time.time()
    rqr_time = end - start
    # print("RQR", end - start)
    start = time.time()
    cfrqr = CFRQR(fname)
    cfrqr_sol = cfrqr.solve()
    end = time.time()
    cfrqr_time = end - start
    # print("CFR-QR", end - start)
    # print(cfrqr_sol)

    # solve qse
    start = time.time()
    gradient = Gradient(fname, 1)
    if not skip and not big:
        nash = gradient.get_general_sum_nash(strategy=True)
    end = time.time()
    nash_time = end - start
    # print("NE", end - start)

    # nash_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1)
    # nash_br = gradient.game.best_response_value(1, nash)
    start = time.time()
    if not big and not med:
        stackelberg = gradient.get_stackelberg_equilibrium(strategy=True)
    end = time.time()
    sse_time = end - start
    # print("SE", end - start)

    # stackelberg_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, stackelberg, 1)
    # stackelberg_br = gradient.game.best_response_value(1, stackelberg)

    start = time.time()
    grad_sol = gradient.solve(gradient.random_strategy())[0]
    # grad_qr = -grad_sol['fun']
    # grad_strat = grad_sol['x']
    # grad_br = gradient.game.best_response_value(1, grad_strat)
    end = time.time()
    grad_time = end - start
    # print("GA", end - start)

    start = time.time()
    if skip or big:
        data2 = combine.compute_whole_space(cfrqr_sol[0], gradient.random_strategy(), 1)
    else:
        data2 = combine.compute_whole_space(cfrqr_sol[0], nash, 1)
    end = time.time()
    comb_time = end - start
    # print("COMB", end - start)

    return rqr_time, cfrqr_time, nash_time, sse_time, grad_time, comb_time


def zero_sum_time(fname):
    start = time.time()
    combine = Combine(fname)
    combine.compute_combine_cfr_for_whole_space()
    end = time.time()
    rqr_time = end - start
    # print("RQR", end - start)
    start = time.time()
    cfrqr = CFRQR(fname)
    cfrqr_sol = cfrqr.solve()
    end = time.time()
    cfrqr_time = end - start
    # print("CFR-QR", end - start)
    # print(cfrqr_sol)

    # solve qse
    start = time.time()
    gradient = Gradient(fname, 1)
    nash = gradient.get_nash()
    end = time.time()
    nash_time = end - start
    # print("NE", end - start)

    # nash_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1)
    # nash_br = gradient.game.best_response_value(1, nash)
    # print("SE", end - start)

    # stackelberg_qr = gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, stackelberg, 1)
    # stackelberg_br = gradient.game.best_response_value(1, stackelberg)

    start = time.time()
    grad_sol = gradient.solve(gradient.random_strategy())[0]
    # grad_qr = -grad_sol['fun']
    # grad_strat = grad_sol['x']
    # grad_br = gradient.game.best_response_value(1, grad_strat)
    end = time.time()
    grad_time = end - start
    # print("GA", end - start)

    start = time.time()
    data2 = combine.compute_whole_space(cfrqr_sol[0], gradient.random_strategy(), 1)
    end = time.time()
    comb_time = end - start
    # print("COMB", end - start)

    return rqr_time, cfrqr_time, nash_time, grad_time, comb_time


def measure_all_times_of_size():
    folders = ["GtD", "MV", "TD", "WoA"]
    times_acc = {}
    for size in [700]:
        print(size)
        times_acc[size] = []
        for fold in folders:
            for game_id in range(10):
                path = "data/testing/general_sum/TimeTest/" + str(size) + "/" + fold + "{:05d}".format(game_id) + ".gbt"
                skip = fold == "TD"
                big = size > 600
                med = size > 600
                times = general_sum_time(path, skip, big, med)
                print(times)
                times_acc[size].append(times)
                save_to_file(times_acc, "data/results/gensumtimes/times_med.ti")


def time_measure_convert():
    for size in range(100, 1500, 100):
        for file in glob("data/testing/general_sum/TimeTest/" + str(size) + "/*"):
            convert_nfg_to_gbt(file, file.replace("nfg", "gbt"))


def measure_all_times_of_size_zero_sum():
    times_acc = {}
    for size in [233, 377]:
        print(size)
        times_acc[size] = []
        for game_id in range(10):
            path = "data/testing/" + str(size) + "x" + str(size) + "/ {:05d}".format(game_id) + ".gbt"
            times = zero_sum_time(path)
            print(times)
            times_acc[size].append(times)
            save_to_file(times_acc, "data/results/zerosumtimes/times_biggest.ti")


def join_dict_files(file1, file2, out_file):
    dict1 = load_from_file(file1)
    dict2 = load_from_file(file2)
    out_dict = {**dict1, **dict2}
    save_to_file(out_dict, out_file)


if __name__ == '__main__':
    # cfrqr("data/testing/5x5/00018.gbt")
    # plot_convergence_curve(load_from_file("data/regret_progression.rpg"))
    # cfrqr("data/100x100.gbt")
    # load_and_print_part()
    # create_prog()
    # generate_nash_values()
    # for a in load_from_file("data/testing/amas_tests/wide5/results.time"):
    #     print(a)
    # print("wide")

    # for i in [100, 200, 400, 600, 900, 1200, 2000]:
    #     time_test_rqr("data/" + str(i) + "x" + str(i))
    # time_test("data/testing/amas_tests/wide5/" + str(i) + "/")
    # GameGenerator.generate_more("data/testing/amas_tests/wide5/" + str(i) + "/", 100, size=[3, i])
    #     GameGenerator.generate_more("data/testing/amas_tests/high/" + str(i) + "/", 100, size=[i, 2])
    # GameGenerator.generate_and_save("data/2000x2000", size=[2000, 2000])
    # gradient = Gradient("data/testing/16x2/00000", 1)
    # game = Game()
    # game.load_from_file('data/2D')
    # game.print()
    # gradient = Gradient("data/testing/2x2/03861", 1)
    # solution = gradient.solve(gradient.random_strategy())
    # print(solution)
    # graph_builder = GraphBuilder('data/testing/general_sum/7opt/3')
    # print(graph_builder.game.payoffs)
    # for i in [0.92]:
    #     graph_builder.graph(1, [i], print_actions=False, fname="testL" + str(i))
    # print(game.action_values(1, [1, 0], 0.1))
    # cfr = CFR("data/2ND")
    # cfr2 = QCFR("data/2ND", rationality=1)
    # cfr3 = CFRQR("data/testing/2x2/00134", rationality=10)
    # game = Game()
    # game.load_from_file("data/3ND_FM")
    # x = cfr3.solve(iterations=100000)
    # print(x)
    # print(game.payoff_based_on_strategy_and_opponent_rationality(1, x, 1))
    # print(game.best_response_value(1, x))
    # print(x)
    # for it in [10,100,1000,10000]:
    #     print("Iterations:",it)
    #     print("QCFR")
    #     cfr2.solve(iterations=it)
    #     print("CFRQR")
    #     cfr3.solve(iterations=it)
    # start_points_tests("01086")
    # generate_nonsymmetric_games(16, 2)
    # generate_all_games()
    # generate_results()
    # work_with_data()
    # print_striped_data()
    # strip_data()
    # print_small_game_results(396)
    # print_pure_values_game(396)
    # print_game_results(396)
    # print(my_graph(qne_values_game("data/results/144x144.qne"), sort=6, xlabels=False))
    # print(my_graph(qne_values("data/results/"), normalize=True, lines=False))
    # my_graph(qne_values_game("data/results/2x2.qne10"), normalize=True)
    # more_local_minima_ratio()
    # work_with_data()
    # my_graph(data_analysis(False, True), False)
    # results_from_pure()
    # reshape_all_pures()
    # print_pure("data/results/8x2.pure")
    # game = Game()
    # game.load_from_file("data/3ND_FM")
    # print(game.best_response_value(1, [0.542, 0.196, 0.262]))
    # my_graph(exploit_values())
    # save_exploit_values()
    # data_size()
    # generate_results()
    # strip_data()
    # generate_qne_results()
    # print_data()
    # print_all_game_values("data/results/2x2", 134, "10")
    # my_graph(extensive_values("small1"), normalize=False, lines=True, sort=0, xlabels=True)
    # my_graph(all_extensive_values(), normalize=False, lines=False, sort=0)
    # game = Game()
    # game.load_from_file("data/2ND")
    # plot_game_graph(game)
    # test_cfrqr_time()
    # print(data_analysis(""))

    # cfrqr = CFRQR("data/3ND_FM")
    # plot_convergence_curve_set("data/results/efg_cfrqr_brsmall_progressionstart.prog", iterations=499)
    # plot_all_convergence_curves("data/results/efg_decompbig_progression1000.prog", iterations=100)
    # plot_all_convergence_curves_br("data/results/br_progression.prog", iterations=100)
    # plot_all_convergence_curves_br_efg("data/results/efg_cfrqr_brsmall_progressionstart.prog")
    # shift_scale_experiment()
    # load_and_print()
    # time_test()
    # GameGenerator.generate_and_save("./data/biggae.gbt", True, [2, 18000])
    # GameGenerator.generate_and_save("./data/biggae1.gbt", True, [18000, 2])
    # time_test()
    # save_to_file(combine_cfr_and_cfrqr("3x3"), "combination3x3.comb")
    # average_with_labes(load_from_file("combination3x3.comb"))
    # can_i_get_to_qse_by_combination("data/testing/13x13/00000.gbt")
    # save_to_file(can_i_get_to_qse_by_combination_bulk("3x3"), "data/results/qse_vs_comb_3x3.qvc")
    # stats_on_combination_vs_qse("3x3")
    # can_i_get_to_qse_by_combination_all()
    # save_to_file(combination_data_for_graph_dir("13x13"), "data/results/combination_data_for_graph_13x13.cdfg")
    # graph_from_comb_data(load_from_file("data/results/comb_data_for_graph_144x144.qvc"), skip=1)
    # exploitaility_vs_exploitation_all(0.5)
    # exploitaility_vs_exploitation_all(2)
    # print(combination_data_for_graph("data/testing/3x3/02600.gbt"))
    # print(load_from_file("data/results/expl_vs_expl144x144.eve")[0])
    # add_game_value_to_expl_vs_exlp("144x144")
    # show_all_graphs_expl_vs_expl_dir_exlp_on_axis("144x1442")
    # show_all_graphs_expl_vs_expl_dir("144x1440.5")
    # graph_from_comb_data(average_of_comb_data(), normalize=False)
    # average_of_comb_data()
    # time_test(["data/2000x2000"])
    # zerosumification_test("data/gg")
    # test_cfrqr_time()
    # GameGenerator.generate_and_save("./data/100x100", True, [100, 100])
    # exploitability_vs_exploitation("data/100x100.gbt", verbose=True)
    # save_to_file(([combination_data_for_graph("data/1200x1200.gbt")], ('ro', 'go', 'yo', 'bo', 'rs', 'gs', 'ys', 'bs'),
    #               ('COMB-QR', 'QSE-QR', 'NASH-QR', 'QNE-QR', 'COMB-BR', 'QSE-BR', 'NASH-BR', 'QNE-BR')),
    #              "data/results/comb_data_for_graph_1200x1200.qvc")
    # graph_from_comb_data(average_of_comb_data(), normalize=False)
    # print(load_from_file("data/results/comb_data_for_graph_144x144.qvc"))
    # for rationality in [0.1, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.5, 1.8, 2.1, 3, 4, 6, 8, 10]:
    #     print(rationality)
    # rationality = 1
    # fname = "data/100x100"
    # my_graph((exploitability_vs_exploitation("data/100x100", rationality=rationality),
    #           ("", "", "r-", "b-")), fname=fname.replace("data/","") + str(rationality) + ".png", lines=False,
    #          hlines=True)
    # compare_combinations('data/testing/2x2/004replace61')
    # graph_builder = GraphBuilder("data/help_game")
    # print(graph_builder.game.payoffs)
    # graph_builder.graph(1, [1], print_actions=False)
    # for i in range(2500):
    #     print(i)
    #     graph_builder = GraphBuilder("data/testing/2x2/" + "{:05d}".format(i))
    #     graph_builder.graph(1, [1], print_actions=True)
    # save_to_file(average_function("16x2", 1, 1), "data/results/average_2x16_function.agf")
    # graph_from_comb_data(([load_from_file("data/results/average_2x16_function.agf")], "r-", "AverageValue"),
    #                      normalize=False)
    # solve_best_ne("data/testing/3x3/00003")
    # comb_data_for_graph_all_dirs()
    # graph_from_comb_data(combination_data_for_graph_dir("test"))
    # loss = True
    # a, b, c, d, e = average_of_comb_data(loss)
    # graph_from_comb_data((a, b, c, d), errs=e, normalize=False, QR=not loss, BR=loss, sub_min=False, sub_max=False,
    #                      points=10)
    # save_game_to_txt("data/2000x2000.gbt", "data/text/massive.in")
    # generate_all_games()
    # cfrqr("data/break4.gbt")
    # gradient("data/testing/233x233/00000.gbt")
    # graph_from_comb_data((combination_data_for_graph("data/testing/general_sum/7opt/7.gbt"),
    #                       ("ro", "go", "yo", "bo", "rs", "gs", "ys", "bs"), (
    #                           "COMB-QR", "QSE-QR", "NASH-QR", "RQR-QR", "COMB-BR", "QSE-BR", "NASH-BR", "RQR-BR")))
    # comb_data_for_graph_all_dirs("377")

    # from multiprocessing import Process
    #
    # dir = "34"
    # ps = []
    # for dir in ["13", "21", "34", "55", "89", "144", "233", "377", "610", "987", "1597"]:
    #     ps.append(Process(target=proces_test, args=(dir,)))
    #     ps[-1].start()
    # for p in ps:
    #     p.join()

    # replace_data()
    # graph_from_times()
    # for i in range(1000):
    #     fname = "data/testing/general_sum/MV/MV{:05d}.gbt".format(i)
    #     general_sum_test(fname)
    # fname = "data/testing.gbt"
    # general_sum_test(fname)
    # gradient = Gradient(fname, 1)
    # print(gradient.get_general_sum_nash(strategy=False))
    # print(gradient.get_stackelberg_equilibrium(strategy=False))
    # print(gradient.get_stackelberg_equilibrium(strategy=True))

    # GameGenerator.generate_more("data/testing/general_sum/3x2/", begin_index=0, end_index=100, zero_sum=False, size=[3, 2], grange=[-10, 10])    game = Game()

    # convert_nfg_to_gbt("data/testing/general_sum/WoA/WoA00377.nfg",
    #                    "data/testing/general_sum/WoA/WoA00377.gbt")
    # save_general_sum_data_all()
    # loss = False
    # a, b, c, d, errs = load_gs_data()
    # graph_from_comb_data((a, b, c), x_labels=d, normalize=False, QR=not loss, BR=loss, sub_min=False, sub_max=False,
    #                      errs=errs, points=12)
    # graph_from_times()
    # avg_br = 0
    # avg_qr = 0
    # for file in glob("data/testing/610x610/*"):
    #     print(file)
    #     gradient = Gradient(file, 1)
    #     nash = gradient.get_nash()[0]
    #     avg_qr += gradient.game.payoff_based_on_strategy_and_opponent_rationality(1, nash, 1)
    #     avg_br += gradient.game.best_response_value(1, nash)
    # print(avg_br / 1000)
    # print(avg_qr / 1000)
    # graph_from_times()
    # plot_strategy_progression(0.01, 100, 1.2, "data/testing/3x3/00008.gbt", 1000)
    # general_sum_data("data/testing/general_sum/BIG/TD/TD00000.gbt")
    # convert_nfg_to_gbt("data/testing/general_sum/BIG/TD/TD00000.nfg","data/testing/general_sum/BIG/TD/TD00000.gbt")

    # time_measure_convert()
    # for i in range(100,1100,100):
    #     print("Size:", i)
    # measure_all_times_of_size()
    # print(load_from_file("data/results/gensumtimes/times_300.ti"))
    # print(load_from_file("data/results/2x2.qne"))
    # print(load_from_file("data/results/nfg_time.time"))
    # for i in [2, 3, 5, 8, 13, 21, 34, 55, 89, 144]:#, 233, 377, 610, 987, 1597]:
    #     print(np.average(load_from_file("data/results_small/" + str(i) + "x" + str(i) + ".str")[0]))
    #     print(np.average(load_from_file("data/results/qne/qne_values_" + str(i) + "x" + str(i) + ".qne"), 0))
    # file_loaded = load_from_file("data/results/gensumtimes/times_all.ti")
    # for key, value in file_loaded.items():
    #     print(key)
    #     print(np.average(value, 0))
    # accumulator = [[],[],[],[],[],[],[]]
    # for key, value in file_loaded.items():
    #     accumulator[0].append(key)
    #     value = np.average(value, 0)
    #     for i in range(1,6):
    #         accumulator[i].append(value[i-1])
    #     accumulator[6].append(value[1]*2+value[5])
    # print(accumulator)
    # with open("data/results/gensumtimes/gs_nfg_times.txt",'w') as file:
    #     for line in accumulator:
    #         for number in line:
    #             file.write(str(number) + " ")
    #         file.write("\n")

    # measure_all_times_of_size()
    # join_dict_files("data/results/gensumtimes/times_smed.ti","data/results/gensumtimes/times_big.ti","data/results/gensumtimes/times_all.ti")
