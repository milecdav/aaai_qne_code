from CFR import CFR
import numpy as np
from game import Game


class CFRQR(CFR):

    def __init__(self, fname, rationality=1):
        self.game = Game()
        self.game.load_from_file(fname)
        # print(self.game.payoffs)
        self.strategy = []
        self.values = []
        self.regrets = []
        self.avg_strategy = []
        self.count = 0
        self.rationality = rationality
        self.value_progression = []
        self.regret_progression = []

    def compute_opponent_strategy(self, iteration, player):
        self.quantal_response(player)
        self.update_average_strategy(iteration, player)

    def quantal_response(self, player):
        action_values = self.game.my_action_values(player, self.strategy[1 - player])
        max_val = np.max(action_values)
        action_sum = np.sum(np.exp(self.rationality * (action_values - max_val)))
        for j in range(self.game.actions[player]):
            self.strategy[player][j] = np.exp(self.rationality * (action_values[j] - max_val)) / action_sum

    def compute_regrets(self, player):
        for i in range(self.game.players):
            value = np.sum(np.asarray(self.values[i]) * np.asarray(self.strategy[i]))
            regrets = self.values[i] - value
            # print(regrets)
            # print(self.regrets)
            self.regrets[i] = self.regrets[i] + regrets
            self.regrets = np.asarray(self.regrets)
            if i == 1:
                self.regret_progression.append(np.sum(np.clip(regrets, 0, np.inf)))
            if i == player:
                # print(self.regrets)
                for j in range(2):
                    self.regrets[j][self.regrets[j] < 0] = 0
            # print(self.regrets)
