from CombineCfrs import Combine
from CFRCOMB import CFRCOMB
import numpy as np

if __name__ == '__main__':
    for p in np.linspace(0, 1, 11):
        rqr = CFRCOMB("data/zerosum/34x34/00005.gbt")
        rqr.set_p(p)
        strategy, qr_val, br_val = rqr.solve()
        print(qr_val, br_val)