from CFR import CFR
import numpy as np
from game import Game


class QCFR(CFR):

    def __init__(self, fname, rationality=1):
        self.game = Game()
        self.game.load_from_file(fname)
        # print(self.game.payoffs)
        self.strategy = []
        self.values = []
        self.regrets = []
        self.avg_strategy = []
        self.count = 0
        self.rationality = rationality

    def compute_opponent_strategy(self, iteration, player):
        regret_sum = np.sum(np.exp((self.rationality * self.regrets[player])/iteration))
        self.quantal_regret_matching(player, regret_sum, iteration)
        self.update_average_strategy(iteration, player)

    def quantal_regret_matching(self, player, regret_sum, iteration):
        for j in range(self.game.actions[player]):
            self.strategy[player][j] = (np.exp(self.rationality * self.regrets[player][j] / iteration)) / regret_sum

    def compute_regrets(self, player):
        for i in range(self.game.players):
            value = np.sum(np.asarray(self.values[i]) * np.asarray(self.strategy[i]))
            regrets = self.values[i] - value
            if player == i:
                regrets[regrets < 0] = 0
            self.regrets[i] = self.regrets[i] + regrets

