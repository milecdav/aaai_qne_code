This is the codebase and data sample for experiments on Extensive-form games.

There are three files to be run - main_speed.py, main_performance.py and main_rqr_fixed_p.py

main_speed.py runs algorithms that we compare on one game and prints the resulting times. It has four arguments:
	-f, --filename 		- specifies the game file that should be loaded. Default is "data/small/00000.gbt"
	-i, --iterations 	- specifies number of iterations for algorithms based on cfr. Default is 1000.
	-r, --rationality 	- specifies rationality of QR player. Default is 1.
	-g, --gurobi		- specifies if there is gurobi in the computer and if true, LP and MILP are run. Default is False.
	
main_performance.py runs the same alforithms but reports expected utility against QR and BR. It has same four arguments as main_speed.py.

main_rqr_fixed_p.py runs only RQR but without the adaptive algorithm and enables to fix the p and report results with that p. It has -f,-i and -r the same an instead of -g it has argument -p, --p_value which sets the p for the algorithm.