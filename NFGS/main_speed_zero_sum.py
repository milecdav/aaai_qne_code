import time
from CombineCfrs import Combine
from CFRQR import CFRQR
from gradient import Gradient
import argparse
from CFRCOMB import CFRCOMB
from CFR import CFR
import tabulate

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='My example explanation')
    parser.add_argument(
        '-f',
        '--filename',
        default="data/zerosum/8x8/00000.gbt",
        help='Name of game file to be used. Format efg or gbt. (default is example game located in data/zerosum/8x8/00000.gbt)'
    )
    parser.add_argument(
        '-i',
        '--iterations',
        default=1000,
        help='Number of iterations for algorithm (default is 1000).',
        type=int
    )
    parser.add_argument(
        '-r',
        '--rationality',
        default=1,
        help='Rationality of the Quantal response opponent (default is 1).',
        type=float
    )
    parser.add_argument(
        '-g',
        '--gurobi',
        default=False,
        help='Is Gurobi available on your system (default is False)',
        type=bool
    )

    args = parser.parse_args()

    fname = args.filename
    iterations = args.iterations
    rationality = args.rationality
    gurobi = args.gurobi

    start = time.time()
    rqr = CFRCOMB(fname)
    rqr.solve(iterations)
    end = time.time()
    rqr_time = end - start
    start = time.time()
    cfrqr = CFRQR(fname)
    cfrqr_sol = cfrqr.solve(iterations)
    end = time.time()
    cfrqr_time = end - start

    gradient = Gradient(fname, rationality)
    if gurobi:
        start = time.time()
        nash = gradient.get_nash()
        end = time.time()
        nash_time = end - start

    start = time.time()
    cfr = CFR(fname)
    cfr_sol = cfr.solve(iterations)
    end = time.time()
    cfr_time = end - start

    start = time.time()
    grad_sol = gradient.solve(gradient.random_strategy())[0]
    end = time.time()
    grad_time = end - start

    start = time.time()
    combine = Combine(fname)
    data2 = combine.compute_whole_space(cfrqr_sol[0], cfr_sol[0], rationality)
    end = time.time()
    comb_time = end - start

    print("Zero-sum nfg times")
    table = [["COMB time:", comb_time + cfr_time + cfrqr_time],
             ["GA time:", grad_time],
             ["RQR time:", rqr_time],
             ["CFR-QR time:", cfrqr_time],
             ["CFR time:", cfr_time]]
    if gurobi:
        table.append(["NASH time:", nash_time])
    print(tabulate.tabulate(table))
